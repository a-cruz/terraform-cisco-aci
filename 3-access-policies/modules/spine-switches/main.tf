terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_spine_profile" "Spine_Switches" {
  for_each = {
    for profile in var.switch_profiles : profile.name => profile
  }
  name        = each.value.name
  description = each.value.description
  annotation  = each.value.annotation
  relation_infra_rs_sp_acc_port_p = [
    for intpro in each.value.interface_profiles :
    "uni/infra/spaccportprof-${intpro}"
  ]
  spine_selector {
    name                    = each.value.spine_selector_name
    switch_association_type = "range"
    node_block {
      name  = "blk${each.value.spine_selector_node_from}"
      from_ = each.value.spine_selector_node_from
      to_   = each.value.spine_selector_node_to
    }
  }
}
variable "switch_profiles" {
  type = list(object({
    name                     = string
    description              = string
    spine_selector_name      = string
    spine_selector_node_from = number
    spine_selector_node_to   = number
    annotation               = string
    interface_profiles       = list(string)
  }))
  default = []
}
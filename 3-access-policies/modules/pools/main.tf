terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_vlan_pool" "VLAN-Pools" {
  for_each = {
    for pool in var.vlan_pools : "${pool.name}" => pool
  }
  name        = each.value.name
  description = each.value.description
  alloc_mode  = each.value.alloc_mode
}

locals {
  encap_blocks = flatten([
    for vpool in var.vlan_pools : [
      for block in vpool.encap_blocks : {
        vpool_name = vpool.name
        alloc_mode = block.allocation_mode
        type       = block.type
        from       = block.from
        to         = block.to
      }
    ]
  ])
}

resource "aci_ranges" "Encap-Block" {
  for_each = {
    for block in local.encap_blocks : "${block.vpool_name}/${block.type}/${block.from}/${block.to}" => block
  }
  depends_on   = [aci_vlan_pool.VLAN-Pools]
  vlan_pool_dn = "uni/infra/vlanns-[${each.value.vpool_name}]-${each.value.alloc_mode}"
  from         = "${each.value.type}-${each.value.from}"
  to           = "${each.value.type}-${each.value.to}"
}
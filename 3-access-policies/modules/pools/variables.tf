variable "vlan_pools" {
  type = list(object({
    name         = string
    description  = string
    alloc_mode   = string
    encap_blocks = list(map(string))
  }))
  default = []
}
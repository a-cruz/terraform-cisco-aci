terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_physical_domain" "PhyDom" {
  for_each = {
    for dom in var.physical_domains : dom.name => dom
  }
  name                      = each.value.name
  relation_infra_rs_vlan_ns = "%{if each.value.vpool != ""}uni/infra/vlanns-[${each.value.vpool}]-${each.value.pool_type}%{else}%{endif}"
  annotation                = each.value.annotation
}

resource "aci_l3_domain_profile" "L3ODom" {
  for_each = {
    for dom in var.external_routed_domains : dom.name => dom
  }
  name                      = each.value.name
  relation_infra_rs_vlan_ns = "%{if each.value.vpool != ""}uni/infra/vlanns-[${each.value.vpool}]-${each.value.pool_type}%{else}%{endif}"
  annotation                = each.value.annotation
}
variable "physical_domains" {
  type = list(map(string))
}

variable "external_routed_domains" {
  type = list(map(string))
}
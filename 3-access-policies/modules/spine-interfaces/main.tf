terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

# SPINE INTERFACE POLICY GROUPS
resource "aci_spine_port_policy_group" "Access-PolGrp" {
  for_each = {
    for pol in var.access_port_policy_groups : pol.name => pol
  }
  name                         = each.value.name
  description                  = each.value.description
  relation_infra_rs_att_ent_p  = "%{if each.value.aaep != ""}uni/infra/attentp-${each.value.aaep}%{else}%{endif}"
  relation_infra_rs_cdp_if_pol = "%{if each.value.cdp_pol != ""}uni/infra/cdpIfP-${each.value.cdp_pol}%{else}%{endif}"
  relation_infra_rs_h_if_pol   = "%{if each.value.link_level_pol != ""}uni/infra/hintfpol-${each.value.link_level_pol}%{else}%{endif}"
  annotation                   = each.value.annotation
}

# SPINE INTERFACE PROFILES
resource "aci_spine_interface_profile" "Profiles" {
  for_each = {
    for profile in var.interface_profiles : profile.name => profile
  }
  name        = each.value.name
  description = each.value.description
  annotation  = each.value.annotation
}

# SPINE INTERFACE PROFILE INTERFACE SELECTORS
locals {
  port_selectors = flatten([
    for intpro_key, intpro in var.interface_profiles : [
      for ps in intpro.port_selectors : {
        intpro      = intpro.name
        ps_name     = ps.name
        description = ps.description
        int_grp     = ps.int_grp
        from_card   = ps.from_card
        from_port   = ps.from_port
        to_card     = ps.to_card
        to_port     = ps.to_port
      }
    ]
  ])
}

resource "aci_spine_access_port_selector" "Access-Port-Selectors" {
  depends_on = [
    aci_spine_port_policy_group.Access-PolGrp
  ]
  for_each = {
    for ps in local.port_selectors : "${ps.intpro}/${ps.ps_name}" => ps
  }
  name                            = each.value.ps_name
  description                     = each.value.description
  spine_interface_profile_dn      = aci_spine_interface_profile.Profiles[each.value.intpro].id
  spine_access_port_selector_type = "range"
  relation_infra_rs_sp_acc_grp    = aci_spine_port_policy_group.Access-PolGrp[each.value.int_grp].id
  annotation                      = ""
}

# SPINE INTERFACE PROFILE INTERFACE SELECTOR PORT BLOCKS
resource "aci_access_port_block" "Access-Port-Blocks" {
  depends_on = [aci_spine_access_port_selector.Access-Port-Selectors]
  for_each = {
    for ps in local.port_selectors : "${ps.intpro}/${ps.ps_name}" => ps
  }
  access_port_selector_dn = aci_spine_access_port_selector.Access-Port-Selectors["${each.value.intpro}/${each.value.ps_name}"].id
  from_card               = each.value.from_card
  from_port               = each.value.from_port
  to_card                 = each.value.to_card
  to_port                 = each.value.to_port
}
terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_lldp_interface_policy" "LLDP-Policies" {
  for_each = {
    for pol in var.lldp_interface : "${pol.name}" => pol
  }
  name        = each.value.name
  description = each.value.description
  admin_rx_st = each.value.receive_state
  admin_tx_st = each.value.transmit_state
}

resource "aci_cdp_interface_policy" "CDP-Policies" {
  for_each = {
    for pol in var.cdp_interface : "${pol.name}" => pol
  }
  name        = each.value.name
  description = each.value.description
  admin_st    = each.value.admin_state
}

resource "aci_lacp_policy" "LACP-Policies" {
  for_each = {
    for pol in var.port_channel : "${pol.name}" => pol
  }
  name        = each.value.name
  description = each.value.description
  mode        = each.value.mode
}

resource "aci_fabric_if_pol" "example" {
  for_each = {
    for pol in var.link_level : "${pol.name}" => pol
  }
  name          = each.value.name
  description   = each.value.description
  auto_neg      = each.value.auto_negotiation
  fec_mode      = each.value.fec_mode
  link_debounce = each.value.debounce_interval_msec
  speed         = each.value.port_speed
}

resource "aci_miscabling_protocol_interface_policy" "miscabling_protocol_policy" {
  for_each = {
    for pol in var.mcp_interface : "${pol.name}" => pol
  }
  name        = each.value.name
  description = each.value.description
  admin_st    = each.value.admin_state
}

resource "aci_spanning_tree_interface_policy" "example" {
  for_each = {
    for pol in var.spanning_tree_interface : "${pol.name}" => pol
  }
  name        = each.value.name
  description = each.value.description
  ctrl        = each.value.enable_bpdu_filter == false && each.value.enable_bpdu_guard == false ? ["unspecified"] : each.value.enable_bpdu_filter == true && each.value.enable_bpdu_guard == true ? ["bpdu-filter", "bpdu-guard"] : each.value.enable_bpdu_filter == true ? ["bpdu-filter"] : ["bpdu-guard"]
}
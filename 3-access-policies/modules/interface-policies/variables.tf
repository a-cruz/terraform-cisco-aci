variable "lldp_interface" {
  type = list(map(string))
}

variable "cdp_interface" {
  type = list(map(string))
}

variable "port_channel" {
  type = list(map(string))
}

variable "link_level" {
  type = list(object({
    name                   = string
    description            = string
    auto_negotiation       = string
    port_speed             = string
    debounce_interval_msec = number
    fec_mode               = string
  }))
}

variable "mcp_interface" {
  type = list(map(string))
}

variable "spanning_tree_interface" {
  type = list(object({
    name               = string
    description        = string
    enable_bpdu_filter = bool
    enable_bpdu_guard  = bool
  }))
}
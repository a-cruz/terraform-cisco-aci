terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_vpc_explicit_protection_group" "VPC-Group" {
  for_each = {
    for group in var.vpc_protection_groups : "${group.name}" => group
  }
  name                             = each.value.name
  switch1                          = each.value.switch1_id
  switch2                          = each.value.switch2_id
  vpc_domain_policy                = each.value.vpc_domain_policy
  vpc_explicit_protection_group_id = each.value.id
}
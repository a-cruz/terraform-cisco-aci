variable "vpc_protection_groups" {
  type = list(object({
    name              = string
    id                = number
    switch1_id        = number
    switch2_id        = number
    vpc_domain_policy = string
  }))
}
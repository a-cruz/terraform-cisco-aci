variable "switch_profiles" {
  type = list(object({
    name                    = string
    description             = string
    leaf_selector_name      = string
    leaf_selector_node_from = number
    leaf_selector_node_to   = number
    annotation              = string
    interface_profiles      = list(string)
  }))
  default = []
}
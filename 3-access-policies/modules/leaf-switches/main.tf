terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_leaf_profile" "leaf_swpro" {
  for_each = {
    for profile in var.switch_profiles : profile.name => profile
  }
  name        = each.value.name
  description = each.value.description
  annotation  = each.value.annotation
  relation_infra_rs_acc_port_p = [
    for intpro in each.value.interface_profiles :
    "uni/infra/accportprof-${intpro}"
  ]
  leaf_selector {
    name                    = each.value.leaf_selector_name
    switch_association_type = "range"
    node_block {
      name  = "blk${each.value.leaf_selector_node_from}"
      from_ = each.value.leaf_selector_node_from
      to_   = each.value.leaf_selector_node_to
    }
  }
}
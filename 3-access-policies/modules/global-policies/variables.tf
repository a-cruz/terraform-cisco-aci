variable "aaeps" {
  type = list(object({
    name             = string
    description      = string
    physical_domains = list(string)
    l3_domains       = list(string)
    annotation       = string
  }))
}

variable "mcp_instance" {
  type = object({
    description             = string
    admin_state             = string
    enable_per_vlan_mcp_pdu = bool
    key                     = string
    loop_detect_mfactor     = number
    loop_prot_port_disable  = bool
    initial_delay           = number
    xmit_freq_sec           = number
    xmit_freq_msec          = number
  })
}

variable "qos_class_preserve_cos" {
  type = bool
}

variable "error_disable_recovery_interval_seconds" {
  type = number
}

variable "recover_from_bpdu_guard" {
  type = bool
}

variable "recover_from_frequent_ep_move" {
  type = bool
}

variable "recover_from_mcp_loop" {
  type = bool
}
terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_attachable_access_entity_profile" "AAEPs" {
  for_each = {
    for aaep in var.aaeps : aaep.name => aaep
  }
  name       = each.value.name
  annotation = each.value.annotation
  relation_infra_rs_dom_p = concat([
    for dom in each.value.physical_domains :
    "uni/phys-${dom}"
    ], [
    for dom in each.value.l3_domains :
    "uni/l3dom-${dom}"
  ])
}
# THIS OBJECT (uni/infra/attentp-{{AAEP}}/gen-default) IS NEEDED TO ATTACH EPGS DIRECTLY TO AAEPS
resource "aci_access_generic" "aaep_default" {
  for_each = {
    for aaep in var.aaeps : aaep.name => aaep
  }
  depends_on                          = [aci_attachable_access_entity_profile.AAEPs]
  attachable_access_entity_profile_dn = "uni/infra/attentp-${each.value.name}"
  name                                = "default"
}

resource "aci_mcp_instance_policy" "mcp_inst_pol" {
  admin_st         = var.mcp_instance.admin_state
  description      = var.mcp_instance.description
  ctrl             = var.mcp_instance.enable_per_vlan_mcp_pdu == true ? ["pdu-per-vlan"] : ["stateful-ha"]
  init_delay_time  = var.mcp_instance.initial_delay
  key              = var.mcp_instance.key
  loop_detect_mult = var.mcp_instance.loop_detect_mfactor
  loop_protect_act = var.mcp_instance.loop_prot_port_disable == true ? "port-disable" : "none"
  tx_freq          = var.mcp_instance.xmit_freq_sec
  tx_freq_msec     = var.mcp_instance.xmit_freq_msec
}

resource "aci_rest" "global_qos_class_preserve_cos" {
  path    = "/api/node/mo/uni/infra/qosinst-default.json"
  payload = <<EOF
    {
        "qosInstPol": {
            "attributes": {
                "dn": "uni/infra/qosinst-default",
                "ctrl": "%{if var.qos_class_preserve_cos}dot1p-preserve%{else}none%{endif}"
            },
            "children": []
        }
    }
    EOF
}

resource "aci_error_disable_recovery" "edr_events" {
  err_dis_recov_intvl = var.error_disable_recovery_interval_seconds
  edr_event {
    event   = "event-mcp-loop"
    recover = var.recover_from_mcp_loop == true ? "yes" : "no"
  }
  edr_event {
    event   = "event-ep-move"
    recover = var.recover_from_frequent_ep_move == true ? "yes" : "no"
  }
  edr_event {
    event   = "event-bpduguard"
    recover = var.recover_from_bpdu_guard == true ? "yes" : "no"
  }
}
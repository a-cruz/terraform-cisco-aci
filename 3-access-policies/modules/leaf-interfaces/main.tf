terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

# LEAF ACCESS PORT POLICY GROUPS
resource "aci_leaf_access_port_policy_group" "Access-PolGrp" {
  for_each = {
    for grp in var.access_port_policy_groups : grp.name => grp
  }
  name                          = each.value.name
  description                   = each.value.description
  relation_infra_rs_att_ent_p   = "%{if each.value.aaep != ""}uni/infra/attentp-${each.value.aaep}%{else}%{endif}"
  relation_infra_rs_cdp_if_pol  = "%{if each.value.cdp_pol != ""}uni/infra/cdpIfP-${each.value.cdp_pol}%{else}%{endif}"
  relation_infra_rs_lldp_if_pol = "%{if each.value.lldp_pol != ""}uni/infra/lldpIfP-${each.value.lldp_pol}%{else}%{endif}"
  relation_infra_rs_stp_if_pol  = "%{if each.value.stp_pol != ""}uni/infra/ifPol-${each.value.stp_pol}%{else}%{endif}"
  relation_infra_rs_h_if_pol    = "%{if each.value.link_level_pol != ""}uni/infra/hintfpol-${each.value.link_level_pol}%{else}%{endif}"
  relation_infra_rs_mcp_if_pol  = "%{if each.value.mcp_pol != ""}uni/infra/mcpIfP-${each.value.mcp_pol}%{else}%{endif}"
  annotation                    = each.value.annotation
}

# PC INTERFACE POLICY GROUPS
resource "aci_leaf_access_bundle_policy_group" "PC-PolGrp" {
  for_each = {
    for grp in var.pc_interface_policy_groups : grp.name => grp
  }
  name                          = each.value.name
  description                   = each.value.description
  lag_t                         = "link"
  relation_infra_rs_att_ent_p   = "%{if each.value.aaep != ""}uni/infra/attentp-${each.value.aaep}%{else}%{endif}"
  relation_infra_rs_cdp_if_pol  = "%{if each.value.cdp_pol != ""}uni/infra/cdpIfP-${each.value.cdp_pol}%{else}%{endif}"
  relation_infra_rs_lldp_if_pol = "%{if each.value.lldp_pol != ""}uni/infra/lldpIfP-${each.value.lldp_pol}%{else}%{endif}"
  relation_infra_rs_lacp_pol    = "%{if each.value.lacp_pol != ""}uni/infra/lacplagp-${each.value.lacp_pol}%{else}%{endif}"
  relation_infra_rs_stp_if_pol  = "%{if each.value.stp_pol != ""}uni/infra/ifPol-${each.value.stp_pol}%{else}%{endif}"
  relation_infra_rs_h_if_pol    = "%{if each.value.link_level_pol != ""}uni/infra/hintfpol-${each.value.link_level_pol}%{else}%{endif}"
  relation_infra_rs_mcp_if_pol  = "%{if each.value.mcp_pol != ""}uni/infra/mcpIfP-${each.value.mcp_pol}%{else}%{endif}"
  annotation                    = each.value.annotation
}

# VPC INTERFACE POLICY GROUPS
resource "aci_leaf_access_bundle_policy_group" "VPC-PolGrp" {
  for_each = {
    for grp in var.vpc_interface_policy_groups : grp.name => grp
  }
  name                          = each.value.name
  description                   = each.value.description
  lag_t                         = "node"
  relation_infra_rs_att_ent_p   = "%{if each.value.aaep != ""}uni/infra/attentp-${each.value.aaep}%{else}%{endif}"
  relation_infra_rs_cdp_if_pol  = "%{if each.value.cdp_pol != ""}uni/infra/cdpIfP-${each.value.cdp_pol}%{else}%{endif}"
  relation_infra_rs_lldp_if_pol = "%{if each.value.lldp_pol != ""}uni/infra/lldpIfP-${each.value.lldp_pol}%{else}%{endif}"
  relation_infra_rs_lacp_pol    = "%{if each.value.lacp_pol != ""}uni/infra/lacplagp-${each.value.lacp_pol}%{else}%{endif}"
  relation_infra_rs_stp_if_pol  = "%{if each.value.stp_pol != ""}uni/infra/ifPol-${each.value.stp_pol}%{else}%{endif}"
  relation_infra_rs_h_if_pol    = "%{if each.value.link_level_pol != ""}uni/infra/hintfpol-${each.value.link_level_pol}%{else}%{endif}"
  relation_infra_rs_mcp_if_pol  = "%{if each.value.mcp_pol != ""}uni/infra/mcpIfP-${each.value.mcp_pol}%{else}%{endif}"
  annotation                    = each.value.annotation
}

# LEAF INTERFACE PROFILES
resource "aci_leaf_interface_profile" "Profiles" {
  for_each = {
    for profile in var.interface_profiles : profile.name => profile
  }
  name        = each.value.name
  description = each.value.description
  annotation  = each.value.annotation
}

# LEAF INTERFACE PROFILE PORT SELECTORS
locals {
  port_selectors = flatten([
    for intpro in var.interface_profiles : [
      for ps in intpro.port_selectors : {
        intpro      = intpro.name
        ps_name     = ps.name
        description = ps.description
        int_grp     = ps.int_grp
        from_card   = ps.from_card
        from_port   = ps.from_port
        to_card     = ps.to_card
        to_port     = ps.to_port
      }
    ]
  ])
}
resource "aci_access_port_selector" "Access-Port-Selectors" {
  depends_on = [
    aci_leaf_access_port_policy_group.Access-PolGrp,
    aci_leaf_access_bundle_policy_group.PC-PolGrp,
    aci_leaf_access_bundle_policy_group.VPC-PolGrp
  ]
  for_each = {
    for ps in local.port_selectors : "${ps.intpro}/${ps.ps_name}" => ps
  }
  name                           = each.value.ps_name
  description                    = each.value.description
  leaf_interface_profile_dn      = aci_leaf_interface_profile.Profiles[each.value.intpro].id
  access_port_selector_type      = "range"
  relation_infra_rs_acc_base_grp = contains(keys(aci_leaf_access_port_policy_group.Access-PolGrp), each.value.int_grp) ? "uni/infra/funcprof/accportgrp-${each.value.int_grp}" : "uni/infra/funcprof/accbundle-${each.value.int_grp}"
  annotation                     = ""
}

# LEAF INTERFACE PROFILE PORT SELECTOR PORT BLOCKS
resource "aci_access_port_block" "Access-Port-Blocks" {
  depends_on = [aci_access_port_selector.Access-Port-Selectors]
  for_each = {
    for ps in local.port_selectors : "${ps.intpro}/${ps.ps_name}" => ps
  }
  access_port_selector_dn = aci_access_port_selector.Access-Port-Selectors["${each.value.intpro}/${each.value.ps_name}"].id
  from_card               = each.value.from_card
  from_port               = each.value.from_port
  to_card                 = each.value.to_card
  to_port                 = each.value.to_port
}
variable "access_port_policy_groups" {
  type = list(map(string))
}

variable "pc_interface_policy_groups" {
  type = list(map(string))
}

variable "vpc_interface_policy_groups" {
  type = list(map(string))
}

variable "interface_profiles" {
  type = list(object({
    name        = string
    description = string
    annotation  = string
    port_selectors = list(object({
      name        = string
      description = string
      int_grp     = string
      from_card   = number
      to_card     = number
      from_port   = number
      to_port     = number
    }))
  }))
}
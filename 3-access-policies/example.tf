module "pools" {
  source = "./modules/pools"

  vlan_pools = [
    {
      name        = "MSite-VPool"
      description = "VLAN Pool for ACI Multi-Site Connectivity"
      alloc_mode  = "static"
      encap_blocks = [
        # Allocation Mode Options: ["static", "dynamic"]
        # Type Options: ["vlan", "vxlan"]
        {
          allocation_mode = "static"
          type            = "vlan"
          from            = 4
          to              = 4
        }
      ]
    },
    {
      name        = "Phys-VPool"
      description = "VLAN Pool for all Physical connectivity"
      alloc_mode  = "static"
      annotation  = ""
      encap_blocks = [
        {
          allocation_mode = "static"
          type            = "vlan"
          from            = 1
          to              = 104
        },
        {
          allocation_mode = "static"
          type            = "vlan"
          from            = 201
          to              = 201
        }
      ]
    },
    {
      name        = "vSphere-VPool"
      description = "VLAN Pool for all virtual/VMware connectivity"
      alloc_mode  = "dynamic"
      annotation  = ""
      encap_blocks = [
        {
          allocation_mode = "dynamic"
          type            = "vlan"
          from            = 1200
          to              = 1299
        }
      ]
    }
  ]
}

module "domains" {
  source     = "./modules/domains"
  depends_on = [module.pools]

  physical_domains = [
    {
      name       = "Phys-Dom"
      vpool      = "Phys-VPool"
      pool_type  = "static"
      annotation = ""
    }
  ]

  external_routed_domains = [
    {
      name       = "L3Out-Dom"
      vpool      = "Phys-VPool"
      pool_type  = "static"
      annotation = ""
    },
    {
      name       = "MSite-L3Out-Dom"
      vpool      = "MSite-VPool"
      pool_type  = "static"
      annotation = ""
    }
  ]
}

module "switch-policies" {
  source = "./modules/switch-policies"

  vpc_protection_groups = [
    {
      name              = "L201-L202-VPCDom"
      id                = 201
      switch1_id        = 201
      switch2_id        = 202
      vpc_domain_policy = "default"
    }
  ]
}

module "interface-policies" {
  source = "./modules/interface-policies"

  lldp_interface = [
    {
      name           = "LLDP-ON"
      description    = "LLDP ON"
      receive_state  = "enabled"
      transmit_state = "enabled"
    },
    {
      name           = "LLDP-OFF"
      description    = "LLDP OFF"
      receive_state  = "disabled"
      transmit_state = "disabled"
    },
    {
      name           = "LLDP-RX-ONLY"
      description    = "LLDP Receive Only"
      receive_state  = "enabled"
      transmit_state = "disabled"
    },
    {
      name           = "LLDP-TX-ONLY"
      description    = "LLDP Transmit Only"
      receive_state  = "disabled"
      transmit_state = "enabled"
    }
  ]

  cdp_interface = [
    {
      name        = "CDP-ON"
      description = "CDP Enabled"
      admin_state = "enabled"
    },
    {
      name        = "CDP-OFF"
      description = "CDP Disabled"
      admin_state = "disabled"
    }
  ]

  port_channel = [
    {
      name        = "LACP-ACTIVE"
      description = "LACP Active (802.3ad)"
      mode        = "active"
    },
    {
      name        = "STATIC-ON"
      description = "Static On Port-Channel"
      mode        = "off"
    },
    {
      name        = "MAC-PIN"
      description = "MAC Pinning Redundancy (No Port-Channel)"
      mode        = "mac-pin"
    }
  ]

  link_level = [
    # FEC Mode Options: ["inherit", "AUTO-FEC", "CL74-FC-FEC", "CL91-RS-FEC", "CONS16-RS-FEC", "DISABLE-FEC", "IEEE-RS-FEC", "KP-FEC"]
    {
      name                   = "SPEED-400G-10ms"
      description            = "Speed 400G, Debounce Interval 10 milliseconds"
      auto_negotiation       = "on"
      port_speed             = "400G"
      debounce_interval_msec = 10
      fec_mode               = "inherit"
    },
    {
      name                   = "SPEED-100G-10ms"
      description            = "Speed 100G, Debounce Interval 10 milliseconds"
      auto_negotiation       = "on"
      port_speed             = "100G"
      debounce_interval_msec = 10
      fec_mode               = "inherit"
    },
    {
      name                   = "SPEED-40G-10ms"
      description            = "Speed 40G, Debounce Interval 10 milliseconds"
      auto_negotiation       = "on"
      port_speed             = "40G"
      debounce_interval_msec = 10
      fec_mode               = "inherit"
    },
    {
      name                   = "SPEED-10G-10ms"
      description            = "Speed 10G, Debounce Interval 10 milliseconds"
      auto_negotiation       = "on"
      port_speed             = "10G"
      debounce_interval_msec = 10
      fec_mode               = "inherit"
    },
    {
      name                   = "SPEED-1G-10ms"
      description            = "Speed 1G, Debounce Interval 10 milliseconds"
      auto_negotiation       = "on"
      port_speed             = "1G"
      debounce_interval_msec = 10
      fec_mode               = "inherit"
    }
  ]

  mcp_interface = [
    {
      name        = "MCP-ON"
      description = "Enable Miscabling Protocol"
      admin_state = "enabled"
    },
    {
      name        = "MCP-OFF"
      description = "Disable Miscabling Protocol"
      admin_state = "enabled"
    }
  ]

  spanning_tree_interface = [
    {
      name               = "STP-BPDU-FILTER-GUARD"
      description        = "Enable BPDU Filter and BPDU Guard"
      enable_bpdu_filter = true
      enable_bpdu_guard  = true
    },
    {
      name               = "STP-BPDU-FILTER"
      description        = "Enable BPDU Filter"
      enable_bpdu_filter = true
      enable_bpdu_guard  = false
    },
    {
      name               = "STP-BPDU-GUARD"
      description        = "Enable BPDU Guard"
      enable_bpdu_filter = false
      enable_bpdu_guard  = true
    },
    {
      name               = "STP-NO-BPDU-FILTER-GUARD"
      description        = "No BPDU filter or BPDU Guard"
      enable_bpdu_filter = false
      enable_bpdu_guard  = false
    }
  ]
}

module "global-policies" {
  depends_on = [module.domains]
  source     = "./modules/global-policies"

  aaeps = [
    {
      name             = "Phys-AAEP"
      description      = "Default AAEP for all physical endpoints"
      physical_domains = ["Phys-Dom"]
      l3_domains       = []
      annotation       = "none"
    },
    {
      name             = "Border-L2Out-AAEP"
      description      = "AAEP for L2 Trunk to core network"
      physical_domains = ["Phys-Dom"]
      l3_domains       = []
      annotation       = ""
    },
    {
      name             = "Border-L3Out-AAEP"
      description      = "AAEP for routed connections to core network"
      physical_domains = ["Phys-Dom"]
      l3_domains       = []
      annotation       = ""
    },
    {
      name             = "Msite-AAEP"
      description      = "AAEP for Multi-Site ISN connections"
      physical_domains = []
      l3_domains       = ["MSite-L3Out-Dom"]
      annotation       = ""
    },
    {
      name             = "UCS-AAEP"
      description      = "AAEP for UCS vSphere compute chassis"
      physical_domains = ["Phys-Dom"]
      l3_domains       = []
      annotation       = ""
    },
    {
      name             = "TEST-AAEP"
      description      = "TESTING"
      physical_domains = ["Phys-Dom"]
      l3_domains       = []
      annotation       = ""
    }
  ]

  mcp_instance = {
    description             = "Enable MCP, per-vlan PDU, no key, default timers"
    admin_state             = "enabled"
    enable_per_vlan_mcp_pdu = true
    key                     = "alnwn3201.1af09"
    loop_detect_mfactor     = 3
    loop_prot_port_disable  = true
    initial_delay           = 180
    xmit_freq_sec           = 2
    xmit_freq_msec          = 0
  }

  qos_class_preserve_cos = true

  error_disable_recovery_interval_seconds = 300
  recover_from_bpdu_guard                 = false
  recover_from_frequent_ep_move           = false
  recover_from_mcp_loop                   = false
}

module "leaf-interfaces" {
  depends_on = [
    module.interface-policies,
    module.global-policies
  ]
  source = "./modules/leaf-interfaces"

  access_port_policy_groups = [
    {
      name           = "ESXI1-L201-P5-IntPolGrp"
      description    = "Access Port Policy Group for ESXi Host 1 VMNIC1"
      annotation     = ""
      aaep           = "Phys-AAEP"
      cdp_pol        = "CDP-ON"
      lldp_pol       = "LLDP-OFF"
      stp_pol        = ""
      link_level_pol = "SPEED-40G-10ms"
      mcp_pol        = "MCP-ON"
      lacp_pol       = ""
    },
    {
      name           = "ESXI1-L202-P5-IntPolGrp"
      description    = "Access Port Policy Group for ESXi Host 1 VMNIC2"
      annotation     = ""
      aaep           = "Phys-AAEP"
      cdp_pol        = "CDP-ON"
      lldp_pol       = "LLDP-OFF"
      stp_pol        = ""
      link_level_pol = "SPEED-40G-10ms"
      mcp_pol        = "MCP-ON"
      lacp_pol       = ""
    }
  ]

  pc_interface_policy_groups = [
    {
      name           = "FW-Active-L201-P6-P7-PC-IntPolGrp"
      description    = "PC Interface Policy Group for Active Firewall ports g0/1 and g0/2"
      annotation     = ""
      aaep           = "Phys-AAEP"
      cdp_pol        = "CDP-ON"
      lldp_pol       = "LLDP-OFF"
      stp_pol        = ""
      link_level_pol = "SPEED-10G-10ms"
      mcp_pol        = "MCP-ON"
      lacp_pol       = "LACP-ACTIVE"
    },
    {
      name           = "FW-Standby-L202-P6-P7-PC-IntPolGrp"
      description    = "PC Interface Policy Group for Standby Firewall ports g0/1 and g0/2"
      annotation     = ""
      aaep           = "Phys-AAEP"
      cdp_pol        = "CDP-ON"
      lldp_pol       = "LLDP-OFF"
      stp_pol        = ""
      link_level_pol = "SPEED-10G-10ms"
      mcp_pol        = "MCP-ON"
      lacp_pol       = "LACP-ACTIVE"
    }
  ]

  vpc_interface_policy_groups = [
    {
      name           = "Border-L2Out-L201-202-P10-VPC-IntPolGrp"
      description    = "VPC Interface Policy Group for Border Connections to DC core switch"
      annotation     = ""
      aaep           = "Border-L2Out-AAEP"
      cdp_pol        = "CDP-ON"
      lldp_pol       = "LLDP-OFF"
      stp_pol        = ""
      link_level_pol = "SPEED-100G-10ms"
      mcp_pol        = "MCP-ON"
      lacp_pol       = "LACP-ACTIVE"
    },
    {
      name           = "UCS1-FIA-L201-202-P8-VPC-IntPolGrp"
      description    = "VPC Interface Policy Group for UCS1 Fabric Interconnect A"
      annotation     = ""
      aaep           = "UCS-AAEP"
      cdp_pol        = "CDP-ON"
      lldp_pol       = "LLDP-OFF"
      stp_pol        = ""
      link_level_pol = "SPEED-40G-10ms"
      mcp_pol        = "MCP-ON"
      lacp_pol       = "LACP-ACTIVE"
    },
    {
      name           = "UCS1-FIB-L201-202-P9-VPC-IntPolGrp"
      description    = "VPC Interface Policy Group for UCS1 Fabric Interconnect B"
      annotation     = ""
      aaep           = "UCS-AAEP"
      cdp_pol        = "CDP-ON"
      lldp_pol       = "LLDP-OFF"
      stp_pol        = ""
      link_level_pol = "SPEED-40G-10ms"
      mcp_pol        = "MCP-ON"
      lacp_pol       = "LACP-ACTIVE"
    }
  ]

  interface_profiles = [
    {
      name        = "L201-IntPro"
      description = "Interface Profile for Leaf 201"
      annotation  = ""
      port_selectors = [
        {
          name        = "Port5"
          description = "Leaf 201 Port 5 using Interface Policy Group ESXI1-L201-P5-IntPolGrp"
          int_grp     = "ESXI1-L201-P5-IntPolGrp"
          from_card   = 1
          to_card     = 1
          from_port   = 5
          to_port     = 5
        },
        {
          name        = "Port6"
          description = "Leaf 201 Port 6 using Interface Policy Group FW-Active-L201-P6-P7-PC-IntPolGrp"
          int_grp     = "FW-Active-L201-P6-P7-PC-IntPolGrp"
          from_card   = 1
          to_card     = 1
          from_port   = 6
          to_port     = 6
        },
        {
          name        = "Port7"
          description = "Leaf 201 Port 7 using Interface Policy Group FW-Active-L201-P6-P7-PC-IntPolGrp"
          int_grp     = "FW-Active-L201-P6-P7-PC-IntPolGrp"
          from_card   = 1
          to_card     = 1
          from_port   = 7
          to_port     = 7
        },
        {
          name        = "Port10"
          description = "Leaf 201 Port 10 using Interface Policy Group Border-L2Out-L201-202-P10-VPC-IntPolGrp"
          int_grp     = "Border-L2Out-L201-202-P10-VPC-IntPolGrp"
          from_card   = 1
          to_card     = 1
          from_port   = 10
          to_port     = 10
        }
      ]
    },
    {
      name        = "L202-IntPro"
      description = "Interface Profile for Leaf 202"
      annotation  = ""
      port_selectors = [
        {
          name        = "Port5"
          description = "Leaf 202 Port 5 using Interface Policy Group ESXI1-L202-P5-IntPolGrp"
          int_grp     = "ESXI1-L202-P5-IntPolGrp"
          from_card   = 1
          to_card     = 1
          from_port   = 5
          to_port     = 5
        },
        {
          name        = "Port6"
          description = "Leaf 202 Port 6 using Interface Policy Group FW-Standby-L202-P6-P7-PC-IntPolGrp"
          int_grp     = "FW-Standby-L202-P6-P7-PC-IntPolGrp"
          from_card   = 1
          to_card     = 1
          from_port   = 6
          to_port     = 6
        },
        {
          name        = "Port7"
          description = "Leaf 202 Port 7 using Interface Policy Group FW-Standby-L202-P6-P7-PC-IntPolGrp"
          int_grp     = "FW-Standby-L202-P6-P7-PC-IntPolGrp"
          from_card   = 1
          to_card     = 1
          from_port   = 7
          to_port     = 7
        },
        {
          name        = "Port10"
          description = "Leaf 202 Port 10 using Interface Policy Group Border-L2Out-L201-202-P10-VPC-IntPolGrp"
          int_grp     = "Border-L2Out-L201-202-P10-VPC-IntPolGrp"
          from_card   = 1
          to_card     = 1
          from_port   = 10
          to_port     = 10
        }
      ]
    }
  ]
}

module "leaf-switches" {
  depends_on = [
    module.leaf-interfaces
  ]
  source = "./modules/leaf-switches"

  switch_profiles = [
    {
      name                    = "Leaf1-SwPro"
      description             = "Switch Profile for Leaf 201"
      leaf_selector_name      = "leaf201"
      leaf_selector_node_from = 201
      leaf_selector_node_to   = 201
      interface_profiles      = ["L201-IntPro"]
      annotation              = ""
    },
    {
      name                    = "Leaf2-SwPro"
      description             = "Switch Profile for Leaf 202"
      leaf_selector_name      = "leaf202"
      leaf_selector_node_from = 202
      leaf_selector_node_to   = 202
      interface_profiles      = ["L202-IntPro"]
      annotation              = ""
    }
  ]
}

module "spine-interfaces" {
  depends_on = [
    module.interface-policies,
    module.global-policies
  ]
  source = "./modules/spine-interfaces"

  access_port_policy_groups = [
    {
      name           = "MSite-ISN-IntPolGrp"
      description    = "Spine Access Port Policy Group for Multi-Site Connectivity"
      aaep           = "Msite-AAEP"
      cdp_pol        = "CDP-ON"
      link_level_pol = "SPEED-40G-10ms"
      annotation     = ""
    }
  ]

  interface_profiles = [
    {
      name        = "S101-IntPro"
      description = "Spine Interface Profile for Spine Switch 101"
      annotation  = ""
      port_selectors = [
        {
          name        = "Port1-51"
          description = "Port 1/51 using policy group MSite-ISN-IntPolGrp"
          int_grp     = "MSite-ISN-IntPolGrp"
          from_card   = 1
          to_card     = 1
          from_port   = 51
          to_port     = 51
        },
        {
          name        = "Port1-52"
          description = "Port 1/52 using policy group MSite-ISN-IntPolGrp"
          int_grp     = "MSite-ISN-IntPolGrp"
          from_card   = 1
          to_card     = 1
          from_port   = 52
          to_port     = 52
        }
      ]
    },
    {
      name        = "S102-IntPro"
      description = "Spine Interface Profile for Spine Switch 102"
      annotation  = ""
      port_selectors = [
        {
          name        = "Port1-51"
          description = "Port 1/51 using policy group MSite-ISN-IntPolGrp"
          int_grp     = "MSite-ISN-IntPolGrp"
          from_card   = 1
          to_card     = 1
          from_port   = 51
          to_port     = 51
        },
        {
          name        = "Port1-52"
          description = "Port 1/52 using policy group MSite-ISN-IntPolGrp"
          int_grp     = "MSite-ISN-IntPolGrp"
          from_card   = 1
          to_card     = 1
          from_port   = 52
          to_port     = 52
        }
      ]
    }
  ]
}

module "spine-switches" {
  depends_on = [
    module.spine-interfaces
  ]
  source = "./modules/spine-switches"

  switch_profiles = [
    {
      name                     = "S101-SwPro"
      description              = "Switch Profile for Spine 101"
      spine_selector_name      = "spine101"
      spine_selector_node_from = 101
      spine_selector_node_to   = 101
      interface_profiles       = ["S101-IntPro"]
      annotation               = ""
    },
    {
      name                     = "S102-SwPro"
      description              = "Switch Profile for Spine 102"
      spine_selector_name      = "spine102"
      spine_selector_node_from = 102
      spine_selector_node_to   = 102
      interface_profiles       = ["S102-IntPro"]
      annotation               = ""
    }
  ]
}
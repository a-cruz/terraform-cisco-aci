terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_vrf" "vrf" {
  for_each = {
    for vrf in var.vrfs : "${vrf.tenant}/${vrf.name}" => vrf
  }
  name                   = each.value.name
  tenant_dn              = "uni/tn-${each.value.tenant}"
  description            = each.value.description
  pc_enf_pref            = each.value.policy_control_enforce == "yes" ? "enforced" : "unenforced"
  pc_enf_dir             = each.value.policy_control_enforcement_direction
  ip_data_plane_learning = each.value.ip_data_plane_learning_enable == "yes" ? "enabled" : "disabled"
}
variable "bfd_interface_policies" {
  type = list(object({
    name                             = string
    description                      = string
    tenant                           = string
    admin_state                      = string
    detection_multiplier             = number
    min_tx_interval                  = number
    min_rx_interval                  = number
    echo_rx_interval                 = number
    echo_admin_state                 = string
    sub_interface_optimization_state = string
  }))
}

variable "ospf_interface_policies" {
  type = list(object({
    name                     = string
    description              = string
    tenant                   = string
    network_type             = string
    priority                 = number
    interface_cost           = string
    hello_interval           = number
    dead_interval            = number
    retransmit_interval      = number
    transmit_delay           = number
    enable_advertise_subnet  = string
    enable_bfd               = string
    enable_mtu_ignore        = string
    enable_passive_interface = string
  }))
}
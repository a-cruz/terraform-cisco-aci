terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

# BFD INTERFACE POLICIES
resource "aci_bfd_interface_policy" "Policies" {
  for_each = {
    for pol in var.bfd_interface_policies : "${pol.tenant}/${pol.name}" => pol
  }
  tenant_dn     = "uni/tn-${each.value.tenant}"
  name          = each.value.name
  description   = each.value.description
  admin_st      = each.value.admin_state
  ctrl          = each.value.sub_interface_optimization_state == "enabled" ? "opt-subif" : null
  detect_mult   = each.value.detection_multiplier
  echo_admin_st = each.value.echo_admin_state
  echo_rx_intvl = each.value.echo_rx_interval
  min_rx_intvl  = each.value.min_rx_interval
  min_tx_intvl  = each.value.min_tx_interval
}

# OSPF INTERFACE POLICIES
resource "aci_ospf_interface_policy" "Policies" {
  for_each = {
    for pol in var.ospf_interface_policies : "${pol.tenant}/${pol.name}" => pol
  }
  tenant_dn = "uni/tn-${each.value.tenant}"
  name      = each.value.name
  cost      = each.value.interface_cost
  ctrl = compact([
    "%{if each.value.enable_advertise_subnet == "yes"}advert-subnet%{else}%{endif}",
    "%{if each.value.enable_bfd == "yes"}bfd%{else}%{endif}",
    "%{if each.value.enable_mtu_ignore == "yes"}mtu-ignore%{else}%{endif}",
    "%{if each.value.enable_passive_interface == "yes"}passive%{else}%{endif}"
  ])
  dead_intvl   = each.value.dead_interval
  hello_intvl  = each.value.hello_interval
  nw_t         = each.value.network_type
  prio         = each.value.priority
  rexmit_intvl = each.value.retransmit_interval
  xmit_delay   = each.value.transmit_delay
}
terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_tenant" "tenant" {
  for_each = {
    for tn in var.tenants : tn.name => tn
  }
  name        = each.value.name
  description = each.value.description
}
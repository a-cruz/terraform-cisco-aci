terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_bridge_domain" "bridge-domain" {
  for_each = {
    for bd in var.bridge_domains : "${bd.tenant}/${bd.name}" => bd
  }
  name                        = each.value.name
  description                 = each.value.description
  tenant_dn                   = "%{if each.value.tenant != ""}uni/tn-${each.value.tenant}%{else}%{endif}"
  relation_fv_rs_ctx          = "%{if each.value.vrf != ""}uni/tn-${each.value.tenant}/ctx-${each.value.vrf}%{else}%{endif}"
  arp_flood                   = each.value.arp_flood
  unk_mac_ucast_act           = each.value.l2_unknown_unicast
  unk_mcast_act               = each.value.l3_unknown_mcast
  multi_dst_pkt_act           = each.value.multi_dest_flooding
  mcast_allow                 = each.value.enable_pim
  unicast_route               = each.value.enable_routing
  relation_fv_rs_bd_to_out    = formatlist("uni/tn-%s/out-%s", each.value.tenant, each.value.l3outs)
  host_based_routing          = each.value.host_routing
  optimize_wan_bandwidth      = each.value.optimize_wan_bandwidth
  ep_clear                    = each.value.ep_clear
  intersite_bum_traffic_allow = each.value.intersite_bum_traffic_allow
  intersite_l2_stretch        = each.value.intersite_l2_stretch
  ip_learning                 = each.value.ip_learning
  limit_ip_learn_to_subnets   = each.value.limit_ip_learn_to_subnets
  mac                         = each.value.mac_address
  ep_move_detect_mode         = each.value.ep_move_detect_mode
}

# BD SUBNETS
locals {
  bd_subnets = flatten([
    for bd in var.bridge_domains : [
      for subnet in bd.subnets : {
        tenant          = bd.tenant
        bd              = bd.name
        ip              = subnet.gateway_ip
        description     = subnet.description
        vip             = subnet.treat_as_vip
        primary         = subnet.make_this_ip_primary
        dp_learning     = subnet.enable_ip_data_plane_learning
        adv_ext         = subnet.scope_flags.advertised_externally
        shared_btw_vrfs = subnet.scope_flags.shared_between_vrfs
        nd_ra_prefix    = subnet.subnet_control_flags.nd_ra_prefix
        svi_gateway     = subnet.subnet_control_flags.no_default_svi_gateway
        querier_ip      = subnet.subnet_control_flags.querier_ip
      }
    ]
  ])
}

resource "aci_subnet" "subnet" {
  depends_on = [aci_bridge_domain.bridge-domain]
  for_each = {
    for subnet in local.bd_subnets : "${subnet.tenant}/${subnet.bd}/${subnet.ip}" => subnet
  }
  parent_dn   = "%{if each.value.bd != ""}uni/tn-${each.value.tenant}/BD-${each.value.bd}%{else}%{endif}"
  description = each.value.description
  ip          = each.value.ip
  ctrl = each.value.nd_ra_prefix == false && each.value.svi_gateway == false && each.value.querier_ip == false ? ["unspecified"] : compact([
    "%{if each.value.nd_ra_prefix}nd%{else}%{endif}",
    "%{if each.value.svi_gateway}no-default-gateway%{else}%{endif}",
    "%{if each.value.querier_ip}querier%{else}%{endif}",
  ])
  preferred = each.value.primary ? "yes" : "no"
  scope = each.value.adv_ext == false && each.value.shared_btw_vrfs == false ? ["private"] : compact([
    "%{if each.value.adv_ext}public%{else}%{endif}",
    "%{if each.value.shared_btw_vrfs}shared%{else}%{endif}"
  ])
  virtual = each.value.vip ? "yes" : "no"
}
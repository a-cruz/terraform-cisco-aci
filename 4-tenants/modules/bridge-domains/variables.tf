variable "bridge_domains" {
  type = list(object({
    name                        = string
    description                 = string
    tenant                      = string
    vrf                         = string
    arp_flood                   = string
    l2_unknown_unicast          = string
    l3_unknown_mcast            = string
    multi_dest_flooding         = string
    enable_pim                  = string
    enable_routing              = string
    l3outs                      = list(string)
    host_routing                = string
    optimize_wan_bandwidth      = string
    ep_clear                    = string
    mac_address                 = string
    intersite_bum_traffic_allow = string
    intersite_l2_stretch        = string
    ip_learning                 = string
    limit_ip_learn_to_subnets   = string
    ep_move_detect_mode         = string
    subnets = list(object({
      gateway_ip                    = string
      description                   = string
      treat_as_vip                  = bool
      make_this_ip_primary          = bool
      enable_ip_data_plane_learning = bool
      scope_flags                   = map(bool)
      subnet_control_flags          = map(bool)
    }))
  }))
}
variable "filters" {
  type = list(object({
    tenant      = string
    description = string
    name        = string
    filter_entries = list(object({
      description      = string
      name             = string
      ether_type       = string
      protocol         = string
      source_from_port = number
      source_to_port   = number
      dest_from_port   = number
      dest_to_port     = number
      stateful         = string
    }))
  }))
  default = []
}

variable "contracts" {
  type = list(object({
    tenant      = string
    description = string
    name        = string
    scope       = string
    contract_subjects = list(object({
      name            = string
      description     = string
      reverse_filter  = string
      subject_filters = list(map(string))
    }))
  }))
  default = []
}
terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

# CONTRACT FILTERS
resource "aci_filter" "Filters" {
  for_each = {
    for filter in var.filters : "${filter.tenant}/${filter.name}" => filter
  }
  tenant_dn   = "uni/tn-${each.value.tenant}"
  description = each.value.description
  name        = each.value.name
}

# CONTRACT FILTER ENTRIES
locals {
  contract_filter_entries = flatten([
    for fil in var.filters : [
      for entry in fil.filter_entries : {
        tenant           = fil.tenant
        filter           = fil.name
        name             = entry.name
        description      = entry.description
        ether_type       = entry.ether_type
        protocol         = entry.protocol
        source_from_port = entry.source_from_port
        source_to_port   = entry.source_to_port
        dest_from_port   = entry.dest_from_port
        dest_to_port     = entry.dest_to_port
        stateful         = entry.stateful
      }
    ]
  ])
}
resource "aci_filter_entry" "Entries" {
  depends_on = [aci_filter.Filters]
  for_each = {
    for fil in local.contract_filter_entries : "${fil.tenant}/${fil.filter}/${fil.name}" => fil
  }
  filter_dn   = aci_filter.Filters["${each.value.tenant}/${each.value.filter}"].id
  description = each.value.description
  name        = each.value.name
  d_from_port = each.value.dest_from_port
  d_to_port   = each.value.dest_to_port
  ether_t     = each.value.ether_type
  prot        = each.value.protocol
  s_from_port = each.value.source_from_port
  s_to_port   = each.value.source_to_port
  stateful    = each.value.stateful
}

# CONTRACTS
resource "aci_contract" "Contracts" {
  for_each = {
    for con in var.contracts : "${con.tenant}/${con.name}" => con
  }
  tenant_dn   = "uni/tn-${each.value.tenant}"
  description = each.value.description
  name        = each.value.name
  scope       = each.value.scope
}

# CONTRACT SUBJECTS
locals {
  contract_subjects = flatten([
    for con in var.contracts : [
      for subj in con.contract_subjects : {
        tenant         = con.tenant
        contract       = con.name
        name           = subj.name
        description    = subj.description
        reverse_filter = subj.reverse_filter
        filters = [for fil_key, fil in subj.subject_filters :
          fil.tenant == "inherit" ? "uni/tn-${con.tenant}/flt-${fil.filter_name}" :
          "uni/tn-${fil.tenant}/flt-${fil.filter_name}"
        ]
      }
    ]
  ])
}
resource "aci_contract_subject" "subject" {
  depends_on = [
    aci_contract.Contracts
  ]
  for_each = {
    for subj in local.contract_subjects : "${subj.tenant}/${subj.contract}/${subj.name}" => subj
  }
  contract_dn                  = aci_contract.Contracts["${each.value.tenant}/${each.value.contract}"].id
  description                  = each.value.description
  name                         = each.value.name
  annotation                   = ""
  cons_match_t                 = "AtleastOne"
  name_alias                   = ""
  prio                         = "unspecified"
  prov_match_t                 = "AtleastOne"
  rev_flt_ports                = each.value.reverse_filter
  target_dscp                  = "unspecified"
  relation_vz_rs_subj_filt_att = each.value.filters
}
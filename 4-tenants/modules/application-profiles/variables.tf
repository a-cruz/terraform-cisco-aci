variable "app_profiles" {
  type = list(object({
    name        = string
    description = string
    tenant      = string
    application_epgs = list(object({
      name                       = string
      description                = string
      bridge_domain              = string
      include_in_preferred_group = bool
      physical_domains           = list(string)
      vmware_vmm_domains         = list(string)
      provided_contracts         = list(string)
      consumed_contracts         = list(string)
      attach_to_aaeps            = list(map(string))
      static_paths               = map(list(map(string)))
    }))
    microseg_epgs = list(object({
      name                       = string
      description                = string
      bridge_domain              = string
      include_in_preferred_group = bool
      physical_domains           = list(string)
      vmware_vmm_domains         = list(string)
      useg_attributes_ips        = list(string)
      provided_contracts         = list(string)
      consumed_contracts         = list(string)
      attach_to_aaeps            = list(map(string))
    }))
  }))
}
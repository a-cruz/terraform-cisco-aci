terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

# APPLICATION PROFILES
resource "aci_application_profile" "application-profile" {
  for_each = {
    for app in var.app_profiles : "${app.tenant}/${app.name}" => app
  }
  name        = each.value.name
  tenant_dn   = "%{if each.value.tenant != ""}uni/tn-${each.value.tenant}%{else}%{endif}"
  description = each.value.description
}

# APPLICATION EPGS
locals {
  app_epgs = flatten([
    for app in var.app_profiles : [
      for epg in app.application_epgs : {
        tenant             = app.tenant
        app                = app.name
        name               = epg.name
        description        = epg.description
        bd                 = epg.bridge_domain
        pref_grp           = epg.include_in_preferred_group
        provided_contracts = epg.provided_contracts
        consumed_contracts = epg.consumed_contracts
      }
    ]
  ])
}
resource "aci_application_epg" "epg" {
  for_each = {
    for epg in local.app_epgs : "${epg.tenant}/${epg.app}/${epg.name}" => epg
  }
  name                   = each.value.name
  description            = each.value.description
  application_profile_dn = "%{if each.value.app != ""}uni/tn-${each.value.tenant}/ap-${each.value.app}%{else}%{endif}"
  pref_gr_memb           = each.value.pref_grp ? "include" : "exclude"
  relation_fv_rs_bd      = "%{if each.value.bd != ""}uni/tn-${each.value.tenant}/BD-${each.value.bd}%{else}%{endif}"
  relation_fv_rs_prov = [
    for con in each.value.provided_contracts :
    length(split("/", con)) == 1 ? "uni/tn-${each.value.tenant}/brc-${con}" :
    "uni/tn-${split("/", con)[0]}/brc-${split("/", con)[1]}"
  ]
  relation_fv_rs_cons = [
    for con in each.value.consumed_contracts :
    length(split("/", con)) == 1 ? "uni/tn-${each.value.tenant}/brc-${con}" :
    "uni/tn-${split("/", con)[0]}/brc-${split("/", con)[1]}"
  ]
}

# ATTACH PHYSICAL DOMAINS TO EPGS
locals {
  epg_phy_doms = flatten([
    for app in var.app_profiles : [
      for epg in app.application_epgs : [
        for dom in epg.physical_domains : {
          tenant = app.tenant
          app    = app.name
          epg    = epg.name
          dom    = dom
        }
      ]
    ]
  ])
}
resource "aci_epg_to_domain" "attach_physical_domain" {
  depends_on = [aci_application_epg.epg]
  for_each = {
    for dom in local.epg_phy_doms : "${dom.tenant}/${dom.app}/${dom.epg}/${dom.dom}" => dom
  }
  application_epg_dn = "%{if each.value.epg != ""}uni/tn-${each.value.tenant}/ap-${each.value.app}/epg-${each.value.epg}%{else}%{endif}"
  tdn                = "uni/phys-${each.value.dom}"
}

# ATTACH VMM DOMAINS TO EPGS
locals {
  epg_vmware_vmm_doms = flatten([
    for app in var.app_profiles : [
      for epg in app.application_epgs : [
        for dom in epg.vmware_vmm_domains : {
          tenant = app.tenant
          app    = app.name
          epg    = epg.name
          dom    = dom
        }
      ]
    ]
  ])
}
resource "aci_epg_to_domain" "attach_vmware_vmm_domain" {
  depends_on = [aci_application_epg.epg]
  for_each = {
    for dom in local.epg_vmware_vmm_doms : "${dom.tenant}/${dom.app}/${dom.epg}/${dom.dom}" => dom
  }
  application_epg_dn = "%{if each.value.epg != ""}uni/tn-${each.value.tenant}/ap-${each.value.app}/epg-${each.value.epg}%{else}%{endif}"
  tdn                = "uni/vmmp-VMware/dom-${each.value.dom}"
}

# ATTACH EPGS TO AAEPS
locals {
  attach_aaeps = flatten([
    for app in var.app_profiles : [
      for epg in app.application_epgs : [
        for aaep in epg.attach_to_aaeps : {
          tenant    = app.tenant
          app       = app.name
          epg       = epg.name
          aaep      = aaep.aaep
          encap     = aaep.encap
          immediacy = aaep.immediacy
          mode      = aaep.mode
        }
      ]
    ]
  ])
}
resource "aci_epgs_using_function" "attach_epgs" {
  depends_on = [
    aci_application_epg.epg
  ]
  for_each = {
    for aaep in local.attach_aaeps : "${aaep.aaep}/${aaep.tenant}/${aaep.app}/${aaep.epg}" => aaep
  }
  access_generic_dn = "uni/infra/attentp-${each.value.aaep}/gen-default"
  tdn               = aci_application_epg.epg["${each.value.tenant}/${each.value.app}/${each.value.epg}"].id
  encap             = each.value.encap
  instr_imedcy      = each.value.immediacy
  mode              = each.value.mode
  primary_encap     = ""
}

# MICROSEG EPGS
locals {
  useg_epgs = flatten([
    for app in var.app_profiles : [
      for epg in app.microseg_epgs : {
        tenant              = app.tenant
        app                 = app.name
        name                = epg.name
        description         = epg.description
        bd                  = epg.bridge_domain
        pref_grp            = epg.include_in_preferred_group
        provided_contracts  = epg.provided_contracts
        consumed_contracts  = epg.consumed_contracts
        useg_attributes_ips = epg.useg_attributes_ips
      }
    ]
  ])
}
resource "aci_application_epg" "useg-epg" {
  for_each = {
    for epg in local.useg_epgs : "${epg.tenant}/${epg.app}/${epg.name}" => epg
  }
  name                   = each.value.name
  description            = each.value.description
  is_attr_based_epg      = "yes"
  application_profile_dn = "%{if each.value.app != ""}uni/tn-${each.value.tenant}/ap-${each.value.app}%{else}%{endif}"
  pref_gr_memb           = each.value.pref_grp ? "include" : "exclude"
  relation_fv_rs_bd      = "%{if each.value.bd != ""}uni/tn-${each.value.tenant}/BD-${each.value.bd}%{else}%{endif}"
  relation_fv_rs_prov = [
    for con in each.value.provided_contracts :
    length(split("/", con)) == 1 ? "uni/tn-${each.value.tenant}/brc-${con}" :
    "uni/tn-${split("/", con)[0]}/brc-${split("/", con)[1]}"
  ]
  relation_fv_rs_cons = [
    for con in each.value.consumed_contracts :
    length(split("/", con)) == 1 ? "uni/tn-${each.value.tenant}/brc-${con}" :
    "uni/tn-${split("/", con)[0]}/brc-${split("/", con)[1]}"
  ]
}

# ATTACH PHYSICAL DOMAINS TO MICROSEG EPGS
locals {
  useg_epg_phy_doms = flatten([
    for app in var.app_profiles : [
      for epg in app.microseg_epgs : [
        for dom in epg.physical_domains : {
          tenant = app.tenant
          app    = app.name
          epg    = epg.name
          dom    = dom
        }
      ]
    ]
  ])
}
resource "aci_epg_to_domain" "useg_attach_physical_domain" {
  depends_on = [aci_application_epg.epg]
  for_each = {
    for dom in local.useg_epg_phy_doms : "${dom.tenant}/${dom.app}/${dom.epg}/${dom.dom}" => dom
  }
  application_epg_dn = "%{if each.value.epg != ""}uni/tn-${each.value.tenant}/ap-${each.value.app}/epg-${each.value.epg}%{else}%{endif}"
  tdn                = "uni/phys-${each.value.dom}"
}

# ATTACH VMM DOMAINS TO MICROSEG EPGS
locals {
  useg_epg_vmware_vmm_doms = flatten([
    for app in var.app_profiles : [
      for epg in app.microseg_epgs : [
        for dom in epg.vmware_vmm_domains : {
          tenant = app.tenant
          app    = app.name
          epg    = epg.name
          dom    = dom
        }
      ]
    ]
  ])
}
resource "aci_epg_to_domain" "useg_attach_vmware_vmm_domain" {
  depends_on = [aci_application_epg.epg]
  for_each = {
    for dom in local.useg_epg_vmware_vmm_doms : "${dom.tenant}/${dom.app}/${dom.epg}/${dom.dom}" => dom
  }
  application_epg_dn = "%{if each.value.epg != ""}uni/tn-${each.value.tenant}/ap-${each.value.app}/epg-${each.value.epg}%{else}%{endif}"
  tdn                = "uni/vmmp-VMware/dom-${each.value.dom}"
}

# ADD USEG IP ATTRIBUTES
resource "aci_rest" "add_ip" {
  depends_on = [aci_epg_to_domain.attach_physical_domain]
  for_each = {
    for epg in local.useg_epgs : "${epg.tenant}/${epg.app}/${epg.name}" => epg
  }
  path    = "/api/node/mo/uni/tn-${each.value.tenant}/ap-${each.value.app}/epg-${each.value.name}/crtrn.json"
  payload = <<EOF
    {
        "fvCrtrn": {
            "attributes": {
                "dn": "uni/tn-${each.value.tenant}/ap-${each.value.app}/epg-${each.value.name}/crtrn",
                "name": "default",
                "match": "any",
                "prec": "0",
                "childAction": "deleteNonPresent"
            },
            "children": [
                %{for i, ip in each.value.useg_attributes_ips}
                {
                    "fvIpAttr": {
                        "attributes": {
                            "dn": "uni/tn-${each.value.tenant}/ap-${each.value.app}/epg-${each.value.name}/crtrn/ipattr-${i}",
                            "name": "${i}",
                            "ip": "${ip}",
                            "usefvSubnet": "false",
                            "childAction": "deleteNonPresent"
                        },
                        "children": []
                    }
                },
                %{endfor}
            ]
        }
    }
    EOF
}

# ATTACH MICROSEG EPGS TO AAEPS
locals {
  useg_attach_aaeps = flatten([
    for app in var.app_profiles : [
      for epg in app.microseg_epgs : [
        for aaep in epg.attach_to_aaeps : {
          tenant    = app.tenant
          app       = app.name
          epg       = epg.name
          aaep      = aaep.aaep
          encap     = aaep.encap
          immediacy = aaep.immediacy
          mode      = aaep.mode
        }
      ]
    ]
  ])
}
resource "aci_epgs_using_function" "useg_attach_epgs" {
  depends_on = [
    aci_application_epg.epg
  ]
  for_each = {
    for aaep in local.useg_attach_aaeps : "${aaep.aaep}/${aaep.tenant}/${aaep.app}/${aaep.epg}" => aaep
  }
  access_generic_dn = "uni/infra/attentp-${each.value.aaep}/gen-default"
  tdn               = aci_application_epg.useg-epg["${each.value.tenant}/${each.value.app}/${each.value.epg}"].id
  encap             = each.value.encap
  instr_imedcy      = each.value.immediacy
  mode              = each.value.mode
  primary_encap     = ""
}

# APPLICATION EPG STATIC PATHS
locals {
  epg_static_paths = concat(flatten([
    for app in var.app_profiles : [
      for epg in app.application_epgs : [
        for path in epg.static_paths.individual_ports : {
          type = "nonvpc"
          tenant     = app.tenant
          app       = app.name
          epg       = epg.name
          pod_id    = path.pod_id
          switch_id = path.switch_id
          interface = path.interface
          encap     = path.encap
          immediacy = path.immediacy
          mode      = path.mode
        }
      ]
    ]
  ]), flatten([
    for app in var.app_profiles : [
      for epg in app.application_epgs : [
        for path in epg.static_paths.port_channels : {
          type = "nonvpc"
          tenant     = app.tenant
          app       = app.name
          epg       = epg.name
          pod_id    = path.pod_id
          switch_id = path.switch_id
          interface = path.interface
          encap     = path.encap
          immediacy = path.immediacy
          mode      = path.mode
        }
      ]
    ]
  ]), flatten([
    for app in var.app_profiles : [
      for epg in app.application_epgs : [
        for path in epg.static_paths.virtual_port_channels : {
          type = "vpc"
          tenant     = app.tenant
          app       = app.name
          epg       = epg.name
          pod_id    = path.pod_id
          switch_id = join("-", [path.switch_1_id, path.switch_2_id])
          interface = path.interface
          encap     = path.encap
          immediacy = path.immediacy
          mode      = path.mode
        }
      ]
    ]
  ]))
}
resource "aci_epg_to_static_path" "static-path" {
  for_each = {
    for path in local.epg_static_paths : "${path.tenant}/${path.app}/${path.epg}/${path.switch_id}/${path.interface}" => path
  }
  application_epg_dn = "uni/tn-${each.value.tenant}/ap-${each.value.app}/epg-${each.value.epg}"
  tdn                = "topology/pod-${each.value.pod_id}/%{if each.value.type == "vpc"}prot%{else}%{endif}paths-${each.value.switch_id}/pathep-[${each.value.interface}]"
  encap              = each.value.encap
  mode               = each.value.mode
  instr_imedcy       = each.value.immediacy
}
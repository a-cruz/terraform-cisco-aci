terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

# L3OUT
resource "aci_l3_outside" "l3out" {
  for_each = {
    for l3o in var.l3outs : "${l3o.tenant}/${l3o.vrf}/${l3o.name}" => l3o
  }
  tenant_dn                    = "uni/tn-${each.value.tenant}"
  description                  = each.value.description
  name                         = each.value.name
  enforce_rtctrl               = each.value.route_control_enforcement_import == "yes" && each.value.route_control_enforcement_export == "no" ? ["import"] : each.value.route_control_enforcement_import == "no" && each.value.route_control_enforcement_export == "yes" ? ["export"] : each.value.route_control_enforcement_import == "yes" && each.value.route_control_enforcement_export == "yes" ? ["import", "export"] : []
  relation_l3ext_rs_l3_dom_att = "%{if each.value.domain != ""}uni/l3dom-${each.value.domain}%{else}%{endif}"
  relation_l3ext_rs_ectx       = "uni/tn-${each.value.tenant}/ctx-${each.value.vrf}"
}

# L3OUT EPGS
locals {
  l3o_epgs = flatten([
    for l3o in var.l3outs : [
      for epg in l3o.epgs : {
        tenant                 = l3o.tenant
        vrf                    = l3o.vrf
        l3o                    = l3o.name
        name                   = epg.name
        description            = epg.description
        preferred_group_enable = epg.preferred_group_enable
        provided_contracts     = epg.provided_contracts
        consumed_contracts     = epg.consumed_contracts
      }
    ]
  ])
}
resource "aci_external_network_instance_profile" "l3out_epg" {
  depends_on = [aci_l3_outside.l3out]
  for_each = {
    for epg in local.l3o_epgs : "${epg.tenant}/${epg.vrf}/${epg.l3o}/${epg.name}" => epg
  }
  l3_outside_dn = aci_l3_outside.l3out["${each.value.tenant}/${each.value.vrf}/${each.value.l3o}"].id
  description   = each.value.description
  name          = element(split("/", each.value.name), length(split("/", each.value.name)) - 1)
  pref_gr_memb  = each.value.preferred_group_enable == "yes" ? "include" : "exclude"
  relation_fv_rs_prov = [
    for con in each.value.provided_contracts :
    # If the contract has a slash that means an explicit Tenant is being provided so we gotta parse it out
    length(split("/", con)) == 1 ?
    "uni/tn-${each.value.tenant}/brc-${con}" :
    "uni/tn-${split("/", con)[0]}/brc-${element(split("/", con), length(split("/", con)) - 1)}"
  ]
  relation_fv_rs_cons = [
    for con in each.value.consumed_contracts :
    length(split("/", con)) == 1 ?
    "uni/tn-${each.value.tenant}/brc-${con}" :
    "uni/tn-${split("/", con)[0]}/brc-${element(split("/", con), length(split("/", con)) - 1)}"
  ]
}

# L3OUT EPG SUBNETS
locals {
  l3o_epg_subnets = flatten([
    for l3o in var.l3outs : [
      for epg in l3o.epgs : [
        for subnet in epg.subnets : {
          tenant = l3o.tenant
          vrf    = l3o.vrf
          l3o    = l3o.name
          epg    = epg.name
          ip     = subnet.ip
          scope = compact([
            "%{if subnet.is_external_subnet_for_external_epg == "yes"}import-security%{else}%{endif}",
            "%{if subnet.is_shared_security_import_subnet == "yes"}shared-security%{else}%{endif}"
          ])
        }
      ]
    ]
  ])
}
resource "aci_l3_ext_subnet" "subnet" {
  depends_on = [aci_external_network_instance_profile.l3out_epg]
  for_each = {
    for subnet in local.l3o_epg_subnets : "${subnet.tenant}/${subnet.vrf}/${subnet.l3o}/${subnet.epg}/${subnet.ip}" => subnet
  }
  external_network_instance_profile_dn = "%{if each.value.l3o != ""}uni/tn-${each.value.tenant}/out-${each.value.l3o}/instP-${each.value.epg}%{else}%{endif}"
  ip                                   = each.value.ip
  scope                                = each.value.scope
}

# L3OUT NODE PROFILES
locals {
  l3o_node_profiles = flatten([
    for l3o in var.l3outs : [
      for np in l3o.node_profiles : {
        tenant      = l3o.tenant
        l3o         = l3o.name
        name        = np.name
        description = np.description
      }
    ]
  ])
}
resource "aci_logical_node_profile" "node_profile" {
  depends_on = [aci_l3_outside.l3out]
  for_each = {
    for np in local.l3o_node_profiles : "${np.tenant}/${np.l3o}/${np.name}" => np
  }
  l3_outside_dn = "%{if each.value.name != ""}uni/tn-${each.value.tenant}/out-${each.value.l3o}%{else}%{endif}"
  description   = each.value.description
  name          = each.value.name
}

# L3OUT NODE PROFILE NODES
locals {
  l3o_nodes = flatten([
    for l3o in var.l3outs : [
      for profile in l3o.node_profiles : [
        for node in profile.nodes : {
          tenant          = l3o.tenant
          l3o             = l3o.name
          node_profile    = profile.name
          router_id       = node.router_id
          create_loopback = node.create_loopback
          pod             = node.pod
          node_id         = node.node_id
        }
      ]
    ]
  ])
}
resource "aci_logical_node_to_fabric_node" "l3out_node" {
  depends_on = [aci_logical_node_profile.node_profile]
  for_each = {
    for node in local.l3o_nodes : "${node.tenant}/${node.l3o}/${node.node_profile}/${node.node_id}" => node
  }
  logical_node_profile_dn = "%{if each.value.node_profile != ""}uni/tn-${each.value.tenant}/out-${each.value.l3o}/lnodep-${each.value.node_profile}%{else}%{endif}"
  tdn                     = join("/", ["topology", "pod-${each.value.pod}", "node-${each.value.node_id}"])
  rtr_id                  = each.value.router_id
  rtr_id_loop_back        = each.value.create_loopback
}

# L3OUT LOGICAL INTERFACE PROFILES
locals {
  l3o_np_interface_profiles = flatten([
    for l3o in var.l3outs : [
      for np in l3o.node_profiles : [
        for intpro in np.interface_profiles : {
          tenant      = l3o.tenant
          l3o         = l3o.name
          np          = np.name
          name        = intpro.name
          description = intpro.description
          bfd_policy  = intpro.bfd_policy
        }
      ]
    ]
  ])
}
resource "aci_logical_interface_profile" "interface_profile" {
  depends_on = [aci_logical_node_profile.node_profile]
  for_each = {
    for intpro in local.l3o_np_interface_profiles : "${intpro.tenant}/${intpro.l3o}/${intpro.np}/${intpro.name}" => intpro
  }
  logical_node_profile_dn = "%{if each.value.np != ""}uni/tn-${each.value.tenant}/out-${each.value.l3o}/lnodep-${each.value.np}%{else}%{endif}"
  description             = each.value.description
  name                    = each.value.name
}

# L3OUT LOGICAL INTERFACE PROFILE PATH ATTACHMENTS (ROUTED INTERFACES)
locals {
  intpro_routed_interfaces = flatten([
    for l3o in var.l3outs : [
      for np in l3o.node_profiles : [
        for intpro in np.interface_profiles : [
          for int in intpro.routed_interfaces : {
            tenant      = l3o.tenant
            l3o         = l3o.name
            np          = np.name
            intpro      = intpro.name
            description = int.description
            pod         = int.pod
            node_id     = int.node_id
            interface   = int.interface
            mac_address = int.mac_address
            mtu         = int.mtu
            ip_address  = int.ip_address
          }
        ]
      ]
    ]
  ])
}
resource "aci_l3out_path_attachment" "routed-interfaces" {
  depends_on = [
    aci_logical_interface_profile.interface_profile
  ]
  for_each = {
    for int in local.intpro_routed_interfaces : "${int.tenant}/${int.l3o}/${int.np}/${int.intpro}/${int.ip_address}" => int
  }
  logical_interface_profile_dn = "uni/tn-${each.value.tenant}/out-${each.value.l3o}/lnodep-${each.value.np}/lifp-${each.value.intpro}"
  target_dn                    = "topology/pod-${each.value.pod}/paths-${each.value.node_id}/pathep-[${each.value.interface}]"
  if_inst_t                    = "l3-port"
  addr                         = each.value.ip_address
  description                  = each.value.description
  autostate                    = "disabled"
  encap                        = "unknown"
  encap_scope                  = "local"
  ipv6_dad                     = "disabled"
  ll_addr                      = "::"
  mac                          = each.value.mac_address == "" ? "00:22:BD:F8:19:FF" : each.value.mac_address
  mode                         = "regular"
  mtu                          = each.value.mtu
  target_dscp                  = "unspecified"
}

# L3OUT LOGICAL INTERFACE PROFILE PATH ATTACHMENTS (ROUTED SUB-INTERFACES)
locals {
  intpro_routed_sub_interfaces = flatten([
    for l3o in var.l3outs : [
      for np in l3o.node_profiles : [
        for intpro in np.interface_profiles : [
          for int in intpro.routed_sub_interfaces : {
            tenant      = l3o.tenant
            l3o         = l3o.name
            np          = np.name
            intpro      = intpro.name
            description = int.description
            pod         = int.pod
            node_id     = int.node_id
            interface   = int.interface
            mac_address = int.mac_address
            mtu         = int.mtu
            ip_address  = int.ip_address
            encap       = int.encap
          }
        ]
      ]
    ]
  ])
}
resource "aci_l3out_path_attachment" "routed-sub-interfaces" {
  depends_on = [
    aci_logical_interface_profile.interface_profile
  ]
  for_each = {
    for int in local.intpro_routed_sub_interfaces : "${int.tenant}/${int.l3o}/${int.np}/${int.intpro}/${int.ip_address}" => int
  }
  logical_interface_profile_dn = "uni/tn-${each.value.tenant}/out-${each.value.l3o}/lnodep-${each.value.np}/lifp-${each.value.intpro}"
  target_dn                    = "topology/pod-${each.value.pod}/paths-${each.value.node_id}/pathep-[${each.value.interface}]"
  if_inst_t                    = "sub-interface"
  addr                         = each.value.ip_address
  description                  = each.value.description
  autostate                    = "disabled"
  encap                        = each.value.encap
  encap_scope                  = "local"
  ipv6_dad                     = "disabled"
  ll_addr                      = "::"
  mac                          = each.value.mac_address == "" ? "00:22:BD:F8:19:FF" : each.value.mac_address
  mode                         = "regular"
  mtu                          = each.value.mtu
  target_dscp                  = "unspecified"
}

# L3OUT LOGICAL INTERFACE PROFILE PATH ATTACHMENTS (NON-VPC SVIS)
locals {
  intpro_non_vpc_svis = concat(flatten([
    for l3o in var.l3outs : [
      for np in l3o.node_profiles : [
        for intpro in np.interface_profiles : [
          for int in intpro.non_vpc_svis : {
            type              = "nonvpc"
            tenant            = l3o.tenant
            l3o               = l3o.name
            np                = np.name
            intpro            = intpro.name
            description       = int.description
            pod               = int.pod
            node_id           = int.node_id
            interface         = int.interface
            side_a_ip_address = null
            side_b_ip_address = null
            mac_address       = int.mac_address
            mtu               = int.mtu
            ip_address        = int.ip_address
            encap             = int.encap
            mode              = int.mode
          }
        ]
      ]
    ]
    ]), flatten([
    for l3o in var.l3outs : [
      for np in l3o.node_profiles : [
        for intpro in np.interface_profiles : [
          for int in intpro.vpc_svis : {
            type              = "vpc"
            tenant            = l3o.tenant
            l3o               = l3o.name
            np                = np.name
            intpro            = intpro.name
            description       = int.description
            pod               = int.pod
            node_id           = "${int.node_1_id}-${int.node_2_id}"
            interface         = int.interface_policy_group
            side_a_ip_address = int.side_a_ip_address
            side_b_ip_address = int.side_b_ip_address
            mac_address       = int.mac_address
            mtu               = int.mtu
            ip_address        = null
            encap             = int.encap
            mode              = int.mode
          }
        ]
      ]
    ]
  ]))
}
resource "aci_l3out_path_attachment" "svis" {
  depends_on = [
    aci_logical_interface_profile.interface_profile
  ]
  for_each = {
    for int in local.intpro_non_vpc_svis : "${int.tenant}/${int.l3o}/${int.np}/${int.intpro}/${int.interface}" => int
  }
  logical_interface_profile_dn = "uni/tn-${each.value.tenant}/out-${each.value.l3o}/lnodep-${each.value.np}/lifp-${each.value.intpro}"
  target_dn                    = "topology/pod-${each.value.pod}/%{if each.value.type == "vpc"}protpaths%{else}paths%{endif}-${each.value.node_id}/pathep-[${each.value.interface}]"
  if_inst_t                    = "ext-svi"
  addr                         = each.value.ip_address
  description                  = each.value.description
  autostate                    = "disabled"
  encap                        = each.value.encap
  encap_scope                  = "local"
  ipv6_dad                     = "disabled"
  ll_addr                      = "::"
  mac                          = each.value.mac_address == "" ? "00:22:BD:F8:19:FF" : each.value.mac_address
  mode                         = each.value.mode
  mtu                          = each.value.mtu
  target_dscp                  = "unspecified"
}

resource "aci_l3out_vpc_member" "vpc_svis_side_a" {
  depends_on = [
    aci_l3out_path_attachment.svis
  ]
  for_each = {
    for int in local.intpro_non_vpc_svis : "${int.tenant}/${int.l3o}/${int.np}/${int.intpro}/${int.interface}" => int if int.type == "vpc"
  }
  leaf_port_dn = "uni/tn-${each.value.tenant}/out-${each.value.l3o}/lnodep-${each.value.np}/lifp-${each.value.intpro}/rspathL3OutAtt-[topology/pod-${each.value.pod}/protpaths-${each.value.node_id}/pathep-[${each.value.interface}]]"
  side         = "A"
  addr         = each.value.side_a_ip_address
  ipv6_dad     = "disabled"
  ll_addr      = "::"
  description  = each.value.description
}

resource "aci_l3out_vpc_member" "vpc_svis_side_b" {
  depends_on = [
    aci_l3out_path_attachment.svis
  ]
  for_each = {
    for int in local.intpro_non_vpc_svis : "${int.tenant}/${int.l3o}/${int.np}/${int.intpro}/${int.interface}" => int if int.type == "vpc"
  }
  leaf_port_dn = "uni/tn-${each.value.tenant}/out-${each.value.l3o}/lnodep-${each.value.np}/lifp-${each.value.intpro}/rspathL3OutAtt-[topology/pod-${each.value.pod}/protpaths-${each.value.node_id}/pathep-[${each.value.interface}]]"
  side         = "B"
  addr         = each.value.side_b_ip_address
  ipv6_dad     = "disabled"
  ll_addr      = "::"
  description  = each.value.description
}

# OSPF CONFIGURATION
resource "aci_l3out_ospf_external_policy" "ospf_config" {
  depends_on = [
    aci_l3_outside.l3out
  ]
  for_each = {
    for l3o in var.l3outs : "${l3o.tenant}/${l3o.name}" => l3o if length(values(l3o.ospf_config)) > 0
  }
  l3_outside_dn = "%{if each.value.name != ""}uni/tn-${each.value.tenant}/out-${each.value.name}%{else}%{endif}"
  area_cost     = each.value.ospf_config.area_cost
  area_ctrl = compact([
    "%{if each.value.ospf_config.send_redistributed_lsas_into_nssa_area == "yes"}redistribute%{else}%{endif}",
    "%{if each.value.ospf_config.originate_summary_lsa == "yes"}summary%{else}%{endif}",
    "%{if each.value.ospf_config.suppress_forarding_address_in_translated_lsa == "yes"}suppress-fa%{else}%{endif}",
  ])
  area_id           = each.value.ospf_config.area_id
  area_type         = each.value.ospf_config.area_type
  multipod_internal = "no"
}

# EIGRP CONFIGURATION
resource "aci_rest" "eigrp" {
  depends_on = [aci_l3_outside.l3out]
  for_each = {
    for l3o in var.l3outs : "${l3o.tenant}/${l3o.name}" => l3o if length(values(l3o.eigrp_config)) > 0
  }
  path    = "/api/node/mo/uni/tn-${each.value.tenant}/out-${each.value.name}/eigrpExtP.json"
  payload = <<EOF
    {
        "eigrpExtP": {
            "attributes": {
                "dn": "uni/tn-${each.value.tenant}/out-${each.value.name}/eigrpExtP",
                "asn": "${each.value.eigrp_config.asn}"
            },
            "children": []
        }
    }
    EOF
}

# BFD POLICY
resource "aci_rest" "attach_bfd_policy" {
  depends_on = [aci_l3_outside.l3out, aci_logical_node_profile.node_profile, aci_logical_interface_profile.interface_profile]
  for_each = {
    for intpro in local.l3o_np_interface_profiles : "${intpro.tenant}/${intpro.l3o}/${intpro.np}/${intpro.name}" => intpro
  }
  path    = "/api/node/mo/uni/tn-${each.value.tenant}/out-${each.value.l3o}/lnodep-${each.value.np}/lifp-${each.value.name}/bfdIfP.json"
  payload = <<EOF
    {
        "bfdIfP": {
            "attributes": {
                "dn": "uni/tn-${each.value.tenant}/out-${each.value.l3o}/lnodep-${each.value.np}/lifp-${each.value.name}/bfdIfP",
                "rn": "bfdIfP"
            },
            "children": [
                {
                    "bfdRsIfPol": {
                        "attributes": {
                            "tnBfdIfPolName": "${each.value.bfd_policy}"
                        },
                        "children": []
                    }
                }
            ]
        }
    }
    EOF
}
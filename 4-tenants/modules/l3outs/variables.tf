variable "l3outs" {
  type = list(object({
    name                             = string
    description                      = string
    tenant                           = string
    vrf                              = string
    route_control_enforcement_import = string
    route_control_enforcement_export = string
    domain                           = string
    igp_routing_protocol             = string
    epgs = list(object({
      name                   = string
      description            = string
      preferred_group_enable = string
      subnets                = list(map(string))
      provided_contracts     = list(string)
      consumed_contracts     = list(string)
    }))
    node_profiles = list(object({
      name        = string
      description = string
      nodes = list(object({
        router_id       = string
        create_loopback = string
        pod             = number
        node_id         = number
      }))
      interface_profiles = list(object({
        name        = string
        description = string
        bfd_policy  = string
        ospf_policy = string
        routed_interfaces = list(object({
          description = string
          pod         = number
          node_id     = number
          interface   = string
          mac_address = string
          mtu         = number
          ip_address  = string
        }))
        routed_sub_interfaces = list(object({
          description = string
          pod         = number
          node_id     = number
          interface   = string
          mac_address = string
          mtu         = number
          ip_address  = string
          encap       = string
        }))
        non_vpc_svis = list(object({
          description = string
          pod         = number
          node_id     = number
          interface   = string
          mac_address = string
          mtu         = number
          ip_address  = string
          encap       = string
          mode        = string
        }))
        vpc_svis = list(object({
          description            = string
          pod                    = number
          node_1_id              = number
          node_2_id              = number
          interface_policy_group = string
          mac_address            = string
          mtu                    = number
          side_a_ip_address      = string
          side_b_ip_address      = string
          encap                  = string
          mode                   = string
        }))
      }))
    }))
    ospf_config  = map(string)
    eigrp_config = map(string)
    bgp_config   = map(string)
  }))
}
terraform {
  required_providers {
    aci = {
      source  = "CiscoDevNet/aci"
      version = "2.6.1"
    }
  }
}

provider "aci" {
  username = var.apic_user
  password = var.apic_pwd
  url      = "https://${var.apic_ip}"
  insecure = true
}
module "tenants" {
  source = "./modules/tenants"

  tenants = [
    {
      name        = "Terraform-MyTenant1"
      description = "A Tenant created by Terraform"
    },
    {
      name        = "Terraform-MyTenant2"
      description = "A Tenant created by Terraform"
    },
    {
      name        = "Terraform-MyTenant3"
      description = "A Tenant created by Terraform"
    }
  ]
}

module "vrfs" {
  depends_on = [
    module.tenants
  ]
  source = "./modules/vrfs"

  vrfs = [
    {
      name                                 = "Prod-VRF"
      tenant                               = "Terraform-MyTenant1"
      description                          = "Production VRF"
      policy_control_enforce               = "yes"
      policy_control_enforcement_direction = "ingress"
      preferred_group_enable               = "yes"
      ip_data_plane_learning_enable        = "yes"
    },
    {
      name                                 = "Dev-VRF"
      tenant                               = "Terraform-MyTenant1"
      description                          = "Development VRF"
      policy_control_enforce               = "yes"
      policy_control_enforcement_direction = "ingress"
      preferred_group_enable               = "yes"
      ip_data_plane_learning_enable        = "yes"
    },
    {
      name                                 = "QA-VRF"
      tenant                               = "Terraform-MyTenant1"
      description                          = "Quality Assurance VRF"
      policy_control_enforce               = "yes"
      policy_control_enforcement_direction = "ingress"
      preferred_group_enable               = "yes"
      ip_data_plane_learning_enable        = "yes"
    }
  ]
}

module "contracts" {
  depends_on = [
    module.tenants
  ]
  source = "./modules/contracts"

  filters = [
    {
      name        = "Web-Fil"
      tenant      = "Terraform-MyTenant1"
      description = "Filter for Web (tcp/80 and tcp/443) traffic"
      filter_entries = [
        {
          name             = "HTTP"
          description      = "HTTPS (TCP/80)"
          ether_type       = "ip"
          protocol         = "tcp"
          source_from_port = null
          source_to_port   = null
          dest_from_port   = 80
          dest_to_port     = 80
          stateful         = "no"
          annotation       = ""
        },
        {
          name             = "HTTPS"
          description      = "HTTPS (TCP/443)"
          ether_type       = "ip"
          protocol         = "tcp"
          source_from_port = null
          source_to_port   = null
          dest_from_port   = 443
          dest_to_port     = 443
          stateful         = "no"
          annotation       = ""
        }
      ]
    }
  ]

  contracts = [
    {
      name        = "Web-Con"
      description = "Web Contract"
      tenant      = "Terraform-MyTenant1"
      scope       = "context"
      contract_subjects = [
        {
          name           = "Any-Subj"
          description    = ""
          reverse_filter = "yes"
          subject_filters = [
            # NOTE: tenant can be set to "inherit" to use the same parent/contract tenant
            {
              filter_name = "Web-Fil"
              tenant      = "inherit"
            },
            {
              filter_name = "default"
              tenant      = "common"
            }
          ]
        }
      ]
    }
  ]
}

module "protocol-policies" {
  depends_on = [
    module.tenants
  ]
  source = "./modules/protocol-policies"

  bfd_interface_policies = [
    {
      name                             = "BFD-ON"
      description                      = "Enable BFD"
      tenant                           = "Terraform-MyTenant1"
      admin_state                      = "enabled"
      detection_multiplier             = 3
      min_tx_interval                  = 50
      min_rx_interval                  = 50
      echo_rx_interval                 = 50
      echo_admin_state                 = "enabled"
      sub_interface_optimization_state = "enabled"
    }
  ]

  ospf_interface_policies = [
    # Network Type Options: ["unspecified", "p2p", "bcast"]
    {
      name                     = "OSPF-P2P-IntPol"
      description              = "Point-to-Point Network type with 10/40 Hello/Dead Timers, Enable BFD"
      tenant                   = "Terraform-MyTenant1"
      network_type             = "p2p"
      priority                 = 1
      interface_cost           = "unspecified"
      hello_interval           = 10
      dead_interval            = 40
      retransmit_interval      = 5
      transmit_delay           = 1
      enable_advertise_subnet  = "yes"
      enable_bfd               = "yes"
      enable_mtu_ignore        = "no"
      enable_passive_interface = "no"
    },
    {
      name                     = "OSPF-Broadcast-IntPol"
      description              = "Broadcast Network Type with 10/40 Hello/Dead Timers, Enable BFD"
      tenant                   = "Terraform-MyTenant1"
      network_type             = "bcast"
      priority                 = 1
      interface_cost           = "unspecified"
      hello_interval           = 10
      dead_interval            = 40
      retransmit_interval      = 5
      transmit_delay           = 1
      enable_advertise_subnet  = "yes"
      enable_bfd               = "yes"
      enable_mtu_ignore        = "no"
      enable_passive_interface = "no"
    }
  ]
}

module "l3outs" {
  depends_on = [
    module.tenants,
    module.vrfs,
    module.contracts,
    module.protocol-policies
  ]
  source = "./modules/l3outs"

  l3outs = [
    {
      name                             = "Prod-OSPF-L3Out"
      description                      = "None"
      tenant                           = "Terraform-MyTenant1"
      vrf                              = "Prod-VRF"
      route_control_enforcement_import = "no"
      route_control_enforcement_export = "yes"
      domain                           = "L3Out-Dom"
      igp_routing_protocol             = "ospf"
      epgs = [
        {
          name                   = "Default-L3Out-EPG"
          description            = "None"
          preferred_group_enable = "yes"
          subnets = [
            {
              ip                                  = "0.0.0.0/1"
              is_external_subnet_for_external_epg = "yes"
              is_shared_security_import_subnet    = "no"
            },
            {
              ip                                  = "128.0.0.0/1"
              is_external_subnet_for_external_epg = "yes"
              is_shared_security_import_subnet    = "no"
            }
          ]
          # NOTE: Contracts can be entered in the format "{{tenant}}/{{contract}}" or the tenant can be omitted (including the slash) to inherit the parent L3O tenant
          provided_contracts = ["common/default"]
          consumed_contracts = ["Web-Con", "common/default"]
        }
      ]
      node_profiles = [
        {
          name        = "Border-leaves-NodePro"
          description = "L3Out Node Profile for Border Leaves"
          nodes = [
            {
              router_id       = "1.1.1.1"
              create_loopback = "yes"
              pod             = 1
              node_id         = 201
            },
            {
              router_id       = "2.2.2.2"
              create_loopback = "yes"
              pod             = 1
              node_id         = 202
            }
          ]
          interface_profiles = [
            {
              name        = "Prod-L3O-IntPro"
              description = ""
              bfd_policy  = "BFD-ON"
              ospf_policy = "OSPF-P2P-IntPol"
              routed_interfaces = [
                # If using a port-channel interface instead of a physical interface, use the Interface Policy Group name instead of port designator (ethx/x) for the interface parameter
                # {
                #   description = "To CORE port ?/?"
                #   pod         = 1
                #   node_id     = 201
                #   interface   = "eth1/9"
                #   mac_address = ""
                #   mtu         = 9216
                #   ip_address  = "10.5.5.1/31"
                # },
                # {
                #   description = "To CORE port ?/?"
                #   pod         = 1
                #   node_id     = 202
                #   interface   = "eth1/9"
                #   mac_address = ""
                #   mtu         = 9216
                #   ip_address  = "10.6.6.1/31"
                # }
              ]
              routed_sub_interfaces = [
                # If using a port-channel interface instead of a physical interface, use the Interface Policy Group name instead of port designator (ethx/x) for the interface parameter
                # Encap should be in the format: "{{type}}-{{encap_id}}" where type is one of ["vlan", "vxlan"]
                # {
                #   description = "To CORE port ?/?"
                #   pod         = 1
                #   node_id     = 201
                #   interface   = "eth1/9"
                #   mac_address = ""
                #   mtu         = 9216
                #   ip_address  = "10.5.5.1/31"
                #   encap       = "vlan-5"
                # },
                # {
                #   description = "To CORE port ?/?"
                #   pod         = 1
                #   node_id     = 202
                #   interface   = "eth1/9"
                #   mac_address = ""
                #   mtu         = 9216
                #   ip_address  = "10.6.6.1/31"
                #   encap       = "vlan-5"
                # }
              ]
              non_vpc_svis = [
                # If using a port-channel interface instead of a physical interface, use the Interface Policy Group name instead of port designator (ethx/x) for the interface parameter
                # Encap should be in the format: "{{type}}-{{encap_id}}" where type is one of ["vlan", "vxlan"]
                # Mode should be one of: ["regular", "untagged"] where "regular" = trunking
                # {
                #   description = "To CORE port ?/?"
                #   pod         = 1
                #   node_id     = 201
                #   interface   = "eth1/9"
                #   mac_address = ""
                #   mtu         = 9216
                #   ip_address  = "10.5.5.1/31"
                #   encap       = "vlan-5"
                #   mode        = "regular"
                # }
              ]
              vpc_svis = [
                # Encap should be in the format: "{{type}}-{{encap_id}}" where type is one of ["vlan", "vxlan"]
                # Mode should be one of: ["regular", "untagged"] where "regular" = trunking
                {
                  description            = "To CORE Switches"
                  pod                    = 1
                  node_1_id              = 201
                  node_2_id              = 202
                  interface_policy_group = "Border-L2Out-L201-202-P10-VPC-IntPolGrp"
                  mac_address            = ""
                  mtu                    = 9216
                  side_a_ip_address      = "10.5.5.2/24"
                  side_b_ip_address      = "10.5.5.3/24"
                  encap                  = "vlan-5"
                  mode                   = "regular"
                }
              ]
            }
          ]
        }
      ]
      ospf_config = {
        # area_id                                      = "51"
        # area_type                                    = "nssa"
        # area_cost                                    = "1"
        # send_redistributed_lsas_into_nssa_area       = "yes"
        # originate_summary_lsa                        = "no"
        # suppress_forarding_address_in_translated_lsa = "no"
      }
      eigrp_config = {
        # asn = "51"
      }
      bgp_config = {}
    }
  ]
}

module "bridge-domains" {
  source = "./modules/bridge-domains"
  depends_on = [
    module.tenants,
    module.vrfs,
    module.l3outs
  ]

  bridge_domains = [
    /*
      ep_move_detect_mode values: ["garp", "disable"] (default is "disable")
      multi_dest_flooding values: ["bd-flood, "encap-flood", "drop"] (default is "bd-flood")
      l2_unknown_unicast values: ["flood", "proxy"] (defaut is "proxy")
      l3_unknown_mcast values: ["flood", "opt-flood"] (default is "flood")
      Most other options are either "yes" or "no"
    */
    {
      name                        = "Web-BD"
      description                 = "Bridge Domain for MyApp Web Tier"
      tenant                      = "Terraform-MyTenant1"
      vrf                         = "Prod-VRF"
      arp_flood                   = "yes"
      l2_unknown_unicast          = "flood"
      l3_unknown_mcast            = "flood"
      multi_dest_flooding         = "bd-flood"
      enable_pim                  = "no"
      enable_routing              = "no"
      l3outs                      = ["Prod-OSPF-L3Out"]
      host_routing                = "no"
      optimize_wan_bandwidth      = "no"
      ep_clear                    = "no"
      mac_address                 = ""
      intersite_bum_traffic_allow = "no"
      intersite_l2_stretch        = "no"
      ip_learning                 = "yes"
      limit_ip_learn_to_subnets   = "yes"
      ep_move_detect_mode         = "disable"
      subnets = [
        {
          gateway_ip                    = "10.8.0.1/24"
          description                   = "Web-BD Default Gateway"
          treat_as_vip                  = false
          make_this_ip_primary          = false
          enable_ip_data_plane_learning = true
          scope_flags = {
            advertised_externally = true
            shared_between_vrfs   = false
          }
          subnet_control_flags = {
            nd_ra_prefix           = true
            no_default_svi_gateway = false
            querier_ip             = false
          }
        },
        {
          gateway_ip                    = "10.8.1.1/24"
          description                   = "Web-BD Default Gateway for Secondary Subnet"
          treat_as_vip                  = false
          make_this_ip_primary          = false
          enable_ip_data_plane_learning = true
          scope_flags = {
            advertised_externally = true
            shared_between_vrfs   = false
          }
          subnet_control_flags = {
            nd_ra_prefix           = true
            no_default_svi_gateway = false
            querier_ip             = false
          }
        }
      ]
    },
    {
      name                        = "App-BD"
      description                 = "Bridge Domain for MyApp App Tier"
      tenant                      = "Terraform-MyTenant1"
      vrf                         = "Prod-VRF"
      arp_flood                   = "yes"
      l2_unknown_unicast          = "flood"
      l3_unknown_mcast            = "flood"
      multi_dest_flooding         = "bd-flood"
      enable_pim                  = "no"
      annotation                  = ""
      enable_routing              = "no"
      l3outs                      = []
      host_routing                = "no"
      optimize_wan_bandwidth      = "no"
      ep_clear                    = "no"
      mac_address                 = ""
      intersite_bum_traffic_allow = "no"
      intersite_l2_stretch        = "no"
      ip_learning                 = "yes"
      limit_ip_learn_to_subnets   = "yes"
      ep_move_detect_mode         = "disable"
      subnets = [
        {
          gateway_ip                    = "10.8.2.1/24"
          description                   = "App-BD Default Gateway"
          treat_as_vip                  = false
          make_this_ip_primary          = false
          enable_ip_data_plane_learning = true
          scope_flags = {
            advertised_externally = true
            shared_between_vrfs   = false
          }
          subnet_control_flags = {
            nd_ra_prefix           = true
            no_default_svi_gateway = false
            querier_ip             = false
          }
        }
      ]
    }
  ]
}

module "application-profiles" {
  source = "./modules/application-profiles"
  depends_on = [
    module.bridge-domains
  ]

  app_profiles = [
    {
      name        = "MyApp1"
      description = "Application profile for Web Store"
      tenant      = "Terraform-MyTenant1"
      application_epgs = [
        {
          name                       = "Web-EPG"
          description                = "Web Endpoint Group"
          bridge_domain              = "Web-BD"
          include_in_preferred_group = true
          physical_domains           = ["Phys-Dom"]
          vmware_vmm_domains         = []
          provided_contracts = [
            /*
              To attach a contract in a different tenant, use the format: {{tenant}}/{{contract}}
              If no slash is detected, the plan will inherit the tenant from the parent application profile
            */
            "Web-Con",
            "common/default"
          ]
          consumed_contracts = ["Web-Con", "common/default"]
          attach_to_aaeps = [
            /*
              immediacy values: ["immediate", "lazy"] (default is "lazy")
              mode values: ["regular", "native", "untagged"] (default is "regular")
              NOTE: For mode values, "regular" = trunk, "native" = 802.1p, & "untagged" = access
              encap value should be in the format: {{type}}-{{encap_id}} where {{type}} is one of ["vlan", "vxlan"]
            */
            {
              aaep      = "Phys-AAEP"
              encap     = "vlan-101"
              immediacy = "immediate"
              mode      = "regular"
            },
            {
              aaep      = "UCS-AAEP"
              encap     = "vlan-101"
              immediacy = "immediate"
              mode      = "regular"
            }
          ]
          static_paths = {
            individual_ports = [
              {
                pod_id    = "1"
                switch_id = "101"
                interface = "eth1/21"
                encap     = "vlan-201"
                immediacy = "immediate"
                mode      = "untagged"
              }
            ]
            port_channels = [
              {
                pod_id    = "1"
                switch_id = "101"
                interface = "FW-Active-L201-P6-P7-PC-IntPolGrp"
                encap     = "vlan-201"
                immediacy = "immediate"
                mode      = "regular"
              }
            ]
            virtual_port_channels = [
              {
                pod_id    = "1"
                switch_1_id = "201"
                switch_2_id = "202"
                interface = "Border-L2Out-L201-202-P10-VPC-IntPolGrp"
                encap     = "vlan-333"
                immediacy = "immediate"
                mode      = "native"
              }
            ]
          }
        }
      ]
      microseg_epgs = [
        # NOTE: The useg match is a single "Match Any" block matching any of the IPs in the "useg_attributes_ips" parameter
        {
          name                       = "Web-uSEG-EPG"
          description                = "Web Microseg Endpoint Group"
          bridge_domain              = "Web-BD"
          include_in_preferred_group = true
          physical_domains           = ["Phys-Dom"]
          vmware_vmm_domains         = []
          useg_attributes_ips        = ["10.0.101.101", "10.0.101.102"]
          provided_contracts = [
            /*
              To attach a contract in a different tenant, use the format: {{tenant}}/{{contract}}
              If no slash is detected, the plan will inherit the tenant from the parent application profile
            */
            "Web-Con",
            "common/default"
          ]
          consumed_contracts = ["Web-Con", "common/default"]
          attach_to_aaeps = [
            /*
              immediacy values: ["immediate", "lazy"] (default is "lazy")
              mode values: ["regular", "native", "untagged"] (default is "regular")
              NOTE: For mode values, "regular" = trunk, "native" = 802.1p, & "untagged" = access
              encap value should be in the format: {{type}}-{{encap_id}} where {{type}} is one of ["vlan", "vxlan"]
            */
            {
              aaep      = "Phys-AAEP"
              encap     = "vlan-101"
              immediacy = "immediate"
              mode      = "regular"
            },
            {
              aaep      = "UCS-AAEP"
              encap     = "vlan-101"
              immediacy = "immediate"
              mode      = "regular"
            }
          ]
        }
      ]
    }
  ]
}
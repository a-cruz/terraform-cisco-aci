variable "apic_ip" {
  description = "The IP address of the target APIC"
  type        = string
}
variable "apic_user" {
  description = "The username of the target APIC"
  type        = string
}
variable "apic_pwd" {
  description = "The password of the apic_user account"
  type        = string
  sensitive   = true
}
variable "physical_teps" {
  type = list(object({
    pod_id   = number
    tep_pool = string
  }))
}
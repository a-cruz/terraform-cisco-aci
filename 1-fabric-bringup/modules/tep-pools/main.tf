terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

# PHYSICAL TEP POOLS (MULTI-POD)
resource "aci_rest" "phy_tep" {
  for_each = {
    for pool in var.physical_teps : "${pool.pod_id}/${pool.tep_pool}" => pool
  }
  path    = "/api/node/mo/uni/controller/setuppol/setupp-${each.value.pod_id}.json"
  payload = <<EOF
    {
        "fabricSetupP": {
            "attributes": {
                "dn": "uni/controller/setuppol/setupp-${each.value.pod_id}",
                "podId": "${each.value.pod_id}",
                "tepPool": "${each.value.tep_pool}",
                "rn": "setupp-${each.value.pod_id}"
            },
            "children": []
        }
    }
    EOF
}
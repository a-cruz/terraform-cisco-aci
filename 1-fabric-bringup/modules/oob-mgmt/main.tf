terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

# STATIC OOB MGMT IPS
resource "aci_static_node_mgmt_address" "node" {
  for_each = {
    for node in var.fabric_members : "${node.pod_id}/${node.node_id}" => node
  }

  management_epg_dn = "uni/tn-mgmt/mgmtp-default/oob-default"
  t_dn              = "topology/pod-${each.value.pod_id}/node-${each.value.node_id}"
  type              = "out_of_band"
  addr              = each.value.ipv4_address
  annotation        = each.value.annotation
  description       = each.value.description
  gw                = each.value.ipv4_gw
  v6_addr           = each.value.ipv6_address
  v6_gw             = each.value.ipv6_gw
}

# OOB MGMT EPG
resource "aci_node_mgmt_epg" "oob_mgmt" {
  type                       = "out_of_band"
  management_profile_dn      = "uni/tn-mgmt/mgmtp-default"
  name                       = "default"
  annotation                 = ""
  name_alias                 = ""
  prio                       = "unspecified"
  relation_mgmt_rs_oo_b_ctx  = "uni/tn-mgmt/ctx-oob"
  relation_mgmt_rs_oo_b_prov = ["uni/tn-common/oobbrc-default"]
  lifecycle {
    ignore_changes = [
      relation_mgmt_rs_oo_b_st_node
    ]
  }
}

# EXTERNAL MANAGEMENT NETWORK INSTANCE PROFILE
resource "aci_rest" "oob_ext_mgmt_inst_pro" {
  path    = "/api/node/mo/uni/tn-mgmt/extmgmt-default/instp-OOB-Mgmt-Inst-Pro.json"
  payload = <<EOF
    {
        "mgmtInstP": {
            "attributes": {
                "dn": "uni/tn-mgmt/extmgmt-default/instp-OOB-Mgmt-Inst-Pro",
                "name": "OOB-Mgmt-Inst-Pro",
                "rn": "instp-OOB-Mgmt-Inst-Pro"
            },
            "children": [
                {
                    "mgmtSubnet": {
                        "attributes": {
                            "dn": "uni/tn-mgmt/extmgmt-default/instp-OOB-Mgmt-Inst-Pro/subnet-[0.0.0.0/0]",
                            "ip": "0.0.0.0/0",
                            "rn": "subnet-[0.0.0.0/0]"
                        },
                        "children": []
                    }
                },
                {
                    "mgmtRsOoBCons": {
                        "attributes": {
                            "tnVzOOBBrCPName": "default"
                        },
                        "children": []
                    }
                }
            ]
        }
    }
    EOF
}
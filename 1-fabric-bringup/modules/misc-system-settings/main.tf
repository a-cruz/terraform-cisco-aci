terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_mgmt_preference" "preference" {
  interface_pref = var.apic_connectivity_preference
}

resource "aci_encryption_key" "key" {
  clear_encryption_key              = "no"
  passphrase                        = var.global_aes_passphrase
  passphrase_key_derivation_version = "v1"
  strong_encryption_enabled         = var.enable_global_aes_passphrase_encryption
}

resource "aci_rest" "control_plane_mtu" {
  path    = "/api/node/mo/uni/infra/CPMtu.json"
  payload = <<EOF
  {
      "infraCPMtuPol": {
          "attributes": {
              "dn": "uni/infra/CPMtu",
              "CPMtu": "${var.control_plane_mtu}"
          },
          "children": []
      }
  }
  EOF
}

resource "aci_coop_policy" "policy" {
  type = var.coop_policy_type
}

resource "aci_port_tracking" "policy" {
  admin_st           = var.port_tracking_state
  delay              = var.port_tracking_delay_restore_timer
  include_apic_ports = var.port_tracking_include_apic_ports
  minlinks           = var.port_tracking_number_ports_trigger
}
variable "apic_connectivity_preference" {
  type = string
}

variable "enable_global_aes_passphrase_encryption" {
  type = string
}

variable "global_aes_passphrase" {
  type      = string
  sensitive = true
}

variable "control_plane_mtu" {
  type = string
}

variable "coop_policy_type" {
  type = string
}

variable "port_tracking_state" {
  type = string
}

variable "port_tracking_delay_restore_timer" {
  type = number
}

variable "port_tracking_number_ports_trigger" {
  type = number
}

variable "port_tracking_include_apic_ports" {
  type = string
}
terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_fabric_wide_settings" "settings" {
  unicast_xr_ep_learn_disable       = var.disable_remote_ep_learning
  enforce_subnet_check              = var.enforce_subnet_check
  validate_overlapping_vlans        = var.enforce_epg_vlan_validation
  domain_validation                 = var.enforce_domain_validation
  opflexp_authenticate_clients      = var.spine_opflex_client_authentication
  leaf_opflexp_authenticate_clients = var.leaf_opflex_client_authentication
  opflexp_use_ssl                   = var.spine_opflex_use_ssl
  leaf_opflexp_use_ssl              = var.leaf_opflex_use_ssl
  opflexp_ssl_protocols             = var.opflex_ssl_versions
  reallocate_gipo                   = var.reallocate_gipo
  restrict_infra_vlan_traffic       = var.restrict_infra_vlan_traffic
  enable_remote_leaf_direct         = var.enable_remote_leaf_direct_traffic_forwarding
}
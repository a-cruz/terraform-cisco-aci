variable "disable_remote_ep_learning" {
  type = string
}
variable "enforce_subnet_check" {
  type = string
}
variable "enforce_epg_vlan_validation" {
  type = string
}
variable "enforce_domain_validation" {
  type = string
}
variable "spine_opflex_client_authentication" {
  type = string
}
variable "leaf_opflex_client_authentication" {
  type = string
}
variable "spine_opflex_use_ssl" {
  type = string
}
variable "leaf_opflex_use_ssl" {
  type = string
}
variable "enable_remote_leaf_direct_traffic_forwarding" {
  type = string
}
variable "opflex_ssl_versions" {
  type = list(string)
}
variable "reallocate_gipo" {
  type = string
}
variable "restrict_infra_vlan_traffic" {
  type = string
}
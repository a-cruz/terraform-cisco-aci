variable "bgp_asn" {
  type = string
}

variable "bgp_route_reflector_switch_ids" {
  type = list(number)
}

variable "isis_mtu" {
  type = number
}
variable "isis_redistribution_metric" {
  type = number
}
variable "isis_lsp_fast_flood_mode" {
  type = string
}
variable "isis_lsp_gen_init_wait" {
  type = number
}
variable "isis_lsp_gen_max_wait" {
  type = number
}
variable "isis_lsp_gen_2nd_wait" {
  type = number
}
variable "isis_spf_comp_freq_init_wait" {
  type = number
}
variable "isis_spf_comp_freq_max_wait" {
  type = number
}
variable "isis_spf_comp_freq_2nd_wait" {
  type = number
}
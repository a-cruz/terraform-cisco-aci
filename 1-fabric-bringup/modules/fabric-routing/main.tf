terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

# BGP ASN
resource "aci_rest" "bgp_asn" {
  path    = "/api/mo/uni/fabric/bgpInstP-default/as.json"
  payload = <<EOF
  {
      "bgpAsP": {
          "attributes": {
              "dn": "uni/fabric/bgpInstP-default/as",
              "asn": "${var.bgp_asn}"
          },
          "children": []
      }
  }
    EOF
}

# BGP ROUTE REFLECTORS
resource "aci_rest" "bgp_rr" {
  depends_on = [aci_rest.bgp_asn]
  count      = length(var.bgp_route_reflector_switch_ids)

  path    = "/api/mo/uni/fabric/bgpInstP-default/rr/node-${var.bgp_route_reflector_switch_ids[count.index]}.json"
  payload = <<EOF
  {
      "bgpRRNodePEp": {
          "attributes": {
              "dn": "uni/fabric/bgpInstP-default/rr/node-${var.bgp_route_reflector_switch_ids[count.index]}",
              "id": "${var.bgp_route_reflector_switch_ids[count.index]}",
              "rn": "node-${var.bgp_route_reflector_switch_ids[count.index]}"
          },
          "children": []
      }
  }
    EOF
}

# ISIS POLICY
resource "aci_isis_domain_policy" "policy" {
  mtu                 = var.isis_mtu
  redistrib_metric    = var.isis_redistribution_metric
  lsp_fast_flood      = var.isis_lsp_fast_flood_mode
  lsp_gen_init_intvl  = var.isis_lsp_gen_init_wait
  lsp_gen_max_intvl   = var.isis_lsp_gen_max_wait
  lsp_gen_sec_intvl   = var.isis_lsp_gen_2nd_wait
  spf_comp_init_intvl = var.isis_spf_comp_freq_init_wait
  spf_comp_max_intvl  = var.isis_spf_comp_freq_max_wait
  spf_comp_sec_intvl  = var.isis_spf_comp_freq_2nd_wait
}
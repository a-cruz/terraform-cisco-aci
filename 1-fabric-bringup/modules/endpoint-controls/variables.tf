variable "endpoint_loop_prot_admin_state" {
  type = string
}
variable "endpoint_loop_detect_int" {
  type = number
}
variable "endpoint_loop_detect_mfactor" {
  type = number
}
variable "endpoint_loop_prot_action" {
  type = string
}

variable "rogue_endpoint_admin_state" {
  type = string
}
variable "rogue_endpoint_detect_int" {
  type = number
}
variable "rogue_endpoint_detect_mfactor" {
  type = number
}
variable "rogue_endpoint_hold_int" {
  type = number
}

variable "endpoint_ip_aging" {
  type = string
}
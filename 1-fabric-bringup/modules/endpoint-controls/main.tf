terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_rest" "endpoint_loop_protection" {
  path    = "/api/node/mo/uni/infra/epLoopProtectP-default.json"
  payload = <<EOF
  {
      "epLoopProtectP": {
          "attributes": {
              "dn": "uni/infra/epLoopProtectP-default",
              "adminSt": "${var.endpoint_loop_prot_admin_state}",
              "loopDetectIntvl": "${var.endpoint_loop_detect_int}",
              "loopDetectMult": "${var.endpoint_loop_detect_mfactor}",
              "action": "${var.endpoint_loop_prot_action}"
          },
          "children": []
      }
  }
  EOF
}

resource "aci_rest" "rogue_endpoint_control" {
  path    = "/api/node/mo/uni/infra/epCtrlP-default.json"
  payload = <<EOF
  {
      "epControlP": {
          "attributes": {
              "dn": "uni/infra/epCtrlP-default",
              "adminSt": "${var.rogue_endpoint_admin_state}",
              "rogueEpDetectIntvl": "${var.rogue_endpoint_detect_int}",
              "rogueEpDetectMult": "${var.rogue_endpoint_detect_mfactor}",
              "holdIntvl": "${var.rogue_endpoint_hold_int}"
          },
          "children": []
      }
  }
  EOF
}

resource "aci_rest" "endpoint_ip_aging" {
  path    = "/api/node/mo/uni/infra/ipAgingP-default.json"
  payload = <<EOF
  {
      "epIpAgingP": {
          "attributes": {
              "dn": "uni/infra/ipAgingP-default",
              "adminSt": "${var.endpoint_ip_aging}"
          },
          "children": []
      }
  }
  EOF
}
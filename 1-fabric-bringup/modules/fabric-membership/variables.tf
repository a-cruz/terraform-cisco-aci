variable "fabric_members" {
  type = list(object({
    name         = string
    serial       = string
    node_id      = string
    pod_id       = number
    role         = string
    ipv4_address = string
    ipv4_gw      = string
    ipv6_address = string
    ipv6_gw      = string
    description  = string
    annotation   = string
  }))
}
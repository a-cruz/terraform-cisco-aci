terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_fabric_node_member" "node" {
  for_each = {
    for node in var.fabric_members : "${node.pod_id}/${node.node_id}" => node if node.role != "apic"
  }
  name    = each.value.name
  serial  = each.value.serial
  node_id = each.value.node_id
  pod_id  = each.value.pod_id
  role    = each.value.role
}
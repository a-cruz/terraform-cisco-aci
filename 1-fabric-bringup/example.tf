module "fabric-bringup" {
  source = "./modules/fabric-membership"

  fabric_members = [
    {
      name         = "APIC1"
      serial       = ""
      node_id      = 1
      pod_id       = 1
      role         = "apic"
      ipv4_address = "192.168.254.1/24"
      ipv4_gw      = "192.168.254.254"
      ipv6_address = ""
      ipv6_gw      = ""
      description  = ""
      annotation   = ""
    },
    {
      name         = "SPINE1"
      serial       = "TEP-1-103"
      node_id      = 101
      pod_id       = 1
      role         = "spine"
      ipv4_address = "192.168.254.201/24"
      ipv4_gw      = "192.168.254.254"
      ipv6_address = ""
      ipv6_gw      = ""
      description  = ""
      annotation   = ""
    },
    {
      name         = "LEAF1"
      serial       = "TEP-1-101"
      node_id      = 201
      pod_id       = 1
      role         = "leaf"
      ipv4_address = "192.168.254.202/24"
      ipv4_gw      = "192.168.254.254"
      ipv6_address = ""
      ipv6_gw      = ""
      description  = ""
      annotation   = ""
    },
    {
      name         = "LEAF2"
      serial       = "TEP-1-102"
      node_id      = 202
      pod_id       = 1
      role         = "leaf"
      ipv4_address = "192.168.254.203/24"
      ipv4_gw      = "192.168.254.254"
      ipv6_address = ""
      ipv6_gw      = ""
      description  = ""
      annotation   = ""
    }
  ]
}

module "oob-management" {
  source = "./modules/oob-mgmt"
  # This module will re-use the fabric_members variables from the fabric-bringup module (saved as an output in that module)
  fabric_members = module.fabric-bringup.fabric_members
}

module "tep-pools" {
  source = "./modules/tep-pools"

  physical_teps = [
    # NOTE: Only one pool per POD can be created. Mask must be 1-23.
    # {
    #   pod_id   = 2
    #   tep_pool = "10.12.0.0/16"
    # }
  ]
}

module "fabric-routing" {
  source = "./modules/fabric-routing"

  bgp_asn                        = 65501
  bgp_route_reflector_switch_ids = [101]

  isis_mtu                     = 1492
  isis_redistribution_metric   = 32
  isis_lsp_fast_flood_mode     = "enabled"
  isis_lsp_gen_init_wait       = 50
  isis_lsp_gen_max_wait        = 8000
  isis_lsp_gen_2nd_wait        = 50
  isis_spf_comp_freq_init_wait = 50
  isis_spf_comp_freq_max_wait  = 8000
  isis_spf_comp_freq_2nd_wait  = 50
}

module "endpoint-controls" {
  source = "./modules/endpoint-controls"

  endpoint_loop_prot_admin_state = "disabled"
  endpoint_loop_detect_int       = 60
  endpoint_loop_detect_mfactor   = 4
  endpoint_loop_prot_action      = "port-disable"
  rogue_endpoint_admin_state     = "enabled"
  rogue_endpoint_detect_int      = 60
  rogue_endpoint_detect_mfactor  = 4
  rogue_endpoint_hold_int        = 1800
  endpoint_ip_aging              = "enabled"
}

module "fabric-wide-settings" {
  source = "./modules/fabric-wide-settings"

  disable_remote_ep_learning                   = "yes"
  enforce_subnet_check                         = "yes"
  enforce_epg_vlan_validation                  = "yes"
  enforce_domain_validation                    = "yes"
  spine_opflex_client_authentication           = "yes"
  leaf_opflex_client_authentication            = "yes"
  spine_opflex_use_ssl                         = "yes"
  leaf_opflex_use_ssl                          = "yes"
  opflex_ssl_versions                          = ["TLSv1.1", "TLSv1.2"]
  reallocate_gipo                              = "no"
  restrict_infra_vlan_traffic                  = "no"
  enable_remote_leaf_direct_traffic_forwarding = "yes"
}

module "misc-system-settings" {
    source = "./modules/misc-system-settings"

    apic_connectivity_preference = "ooband"

    enable_global_aes_passphrase_encryption = "yes"
    # NOTE: global_aes_passphrase variable is not supplied here. It can be supplied via environment variable or via cli at runtime.
    global_aes_passphrase = var.global_aes_passphrase

    control_plane_mtu = 9000

    coop_policy_type = "strict"

    port_tracking_state                = "on"
    port_tracking_delay_restore_timer  = 120
    port_tracking_number_ports_trigger = 0
    port_tracking_include_apic_ports   = "yes"
}
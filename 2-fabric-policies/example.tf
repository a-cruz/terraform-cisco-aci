module "pod-policies" {
  source = "./modules/pod-policies"

  date_and_time_policies = [
    {
      name                 = "NTP-On-NoAuth-DateTime-Pol"
      admin_state          = "enabled"
      server_state         = "disabled"
      master_mode          = "disabled"
      stratum_value        = 8
      authentication_state = "disabled"
      ntp_auth_keys = [
        # Auth type should be one of ["sha1", "md5"]
        {
          id        = 1
          key       = "testtesttest"
          auth_type = "sha1"
          trusted   = true
        },
        {
          id        = 2
          key       = "testtesttest"
          auth_type = "md5"
          trusted   = false
        }
      ]
      ntp_servers = [
        {
          server            = "us.pool.org"
          description       = ""
          preferred         = true
          min_poll_interval = 4
          max_poll_interval = 6
          mgmt_epg          = "default"
        },
        {
          server            = "10.0.0.1"
          description       = "Mine"
          preferred         = false
          min_poll_interval = 4
          max_poll_interval = 6
          mgmt_epg          = "default"
        }
      ]
    }
  ]

  snmp_policies = [
    {
      name        = "Prod-SNMP-Pol"
      description = "Some Description"
      admin_state = "enabled"
      community_strings = [
        {
          community_string = "mystring"
          description      = "A description for my string"
        },
        {
          community_string = "anotherstring"
          description      = "Another description for my string"
        }
      ]
      snmp_v3_users = [
        # Privacy Types: ["aes-128", "none"]
        # Auth Types: ["hmac-sha1-96", "hmac-md5-96", "hmac-sha2-224", "hmac-sha2-256", "hmac-sha2-384", "hmac-sha2-512"]
        {
          name         = "MySNMPv3User"
          privacy_type = "aes-128"
          privacy_key  = "blahblahblah"
          auth_type    = "hmac-sha2-256"
          auth_key     = "blahblahblah"
        },
        {
          name         = "AnotherSNMPv3User"
          privacy_type = "none"
          privacy_key  = ""
          auth_type    = "hmac-sha2-512"
          auth_key     = "blahblahblah"
        }
      ]
      client_group_profiles = [
        {
          name        = "Some_Client_Group"
          description = "My CG Profile 1"
          mgmt_epg    = "default"
          client_entries = [
            {
              name       = "SolarWinds"
              ip_address = "10.10.10.10"
            },
            {
              name       = "SolarWinds2"
              ip_address = "10.10.10.11"
            }
          ]
        }
      ]
      trap_forward_servers = [
        {
          ip_address = "5.0.0.1"
          port       = 81
        },
        {
          ip_address = "5.0.0.2"
          port       = 82
        }
      ]
    }
  ]
}

module "macsec-policies" {
  source = "./modules/macsec-policies"

  macsec_parameters = [
    # Cipher Suite Options: ["gcm-aes-128", "gcm-aes-xpn-128", "gcm-aes-256", "gcm-aes-xpn-256"] (XPN = With Extended Packet Numbering)
    # Security Policy Options: ["should-secure", "must-secure"]
    {
      name            = "Macsec256XPN-ShouldSecure-ParaPol"
      description     = "A description"
      cipher_suite    = "gcm-aes-xpn-256"
      window_size     = 64
      security_policy = "should-secure"
    },
    {
      name            = "Macsec128-MustSecure-ParaPol"
      description     = "A description"
      cipher_suite    = "gcm-aes-128"
      window_size     = 64
      security_policy = "must-secure"
    }
  ]

  key_chains = [
    {
      name        = "AKeyChain"
      description = "A description"
      key_policies = [
        # Start/end times can be "now" (start) or "infinite" (end) or the format: YYYY-MM-DD HH:MM:SS
        # Key name is up to 64 hexadecimal characters
        # Pre-Shared Key must be either 32 or 64 hexadecimal characters
        {
          name           = "MyMACsecKeyPolicy"
          description    = "a description"
          key_name       = "beefbeefaaaa"
          pre_shared_key = "beefbeefbeefbeefbeefbeefbeefbeefbeefbeefbeefbeefbeefbeefbeefbeef"
          start_time     = "now"
          end_time       = "infinite"
        },
        {
          name           = "MyOtherMACsecKeyPolicy"
          description    = "anoter description"
          key_name       = "beefbeefbbbb"
          pre_shared_key = "beefbeefbeefbeefbeefbeefbeeffeebbeefbeefbeefbeefbeefbeefbeeffeeb"
          start_time     = "now"
          end_time       = "infinite"
        }
      ]
    },
    {
      name         = "AnotherKeyChain"
      description  = "Another description"
      key_policies = []
    }
  ]

  interface_policies = [
    {
      name              = "MyMACsecIntPol"
      description       = "Some description"
      admin_state       = "enabled"
      generate_keys     = false
      parameters_policy = "Macsec256XPN-ShouldSecure-ParaPol"
      keychain          = "AKeyChain"
    },
    {
      name              = "MyMACsecIntPol2"
      description       = "Another description"
      admin_state       = "enabled"
      generate_keys     = true
      parameters_policy = "Macsec256XPN-ShouldSecure-ParaPol"
      keychain          = null
    }
  ]
}

module "global-policies" {
  source = "./modules/global-policies"

  fabric_l2_mtu = 9216
}

module "interface-policies" {
  source = "./modules/interface-policies"

  l3_interface_policies = [
    {
      name                          = "BFD-ISIS-Pol-Enabled-L3IntPol"
      description                   = "Enable BFD ISIS Policy Configuration"
      bfd_isis_policy_configuration = "enabled"
    },
    {
      name                          = "BFD-ISIS-Pol-Disabled-L3IntPol"
      description                   = "Disable BFD ISIS Policy Configuration"
      bfd_isis_policy_configuration = "disabled"
    }
  ]

  link_level_policies = [
    {
      name                        = "0msDebounce_LLPol"
      description                 = "0 millisecond Debounce Link Level Policy"
      link_debounce_msec_interval = 0
    },
    {
      name                        = "3msDebounce_LLPol"
      description                 = "3 millisecond Debounce Link Level Policy"
      link_debounce_msec_interval = 3
    }
  ]

  link_flap_policies = [
    # Max Flaps allowed range: 2 - 30
    # Time allowed range: 5 - 420 (seconds)
    {
      name                       = "10FlapsOver200"
      description                = "Trigger at 10 Flaps over 200 time interval"
      max_flaps_allowed_per_time = 10
      time_allowed_seconds       = 200
    },
    {
      name                       = "30FlapsOver420"
      description                = "Trigger at 30 Flaps over 420 time interval"
      max_flaps_allowed_per_time = 30
      time_allowed_seconds       = 420
    }
  ]
}

module "pod-profiles" {
  source = "./modules/pod-profiles"

  pod_policy_groups = [
    {
      name               = "All-pods-PodPolGrp"
      description        = "All Pods in the environment"
      date_time_policy   = "NTP-On-NoAuth-DateTime-Pol"
      isis_policy        = "default"
      coop_group_policy  = "default"
      bgp_rr_policy      = "default"
      mgmt_access_policy = "default"
      snmp_policy        = "Prod-SNMP-Pol"
      macsec_policy      = "MyMACsecIntPol"
    }
  ]

  pod_profiles = [
    {
      name        = "default"
      description = "All Pods Profile"
      pod_selectors = [
        # type is one of: ["ALL", "range"]
        # range is one of: ["ALL", <Pod ID Range>]
        {
          name         = "default"
          type         = "ALL"
          range        = "1-2"
          policy_group = "All-pods-PodPolGrp"
        }
      ]
    }
  ]
}

module "leaf-interfaces" {
  source = "./modules/leaf-interfaces"

  policy_groups = [
    {
      name                = "MyLeaf-PolGrp"
      description         = "My Leaf Fabric Port Policy Group"
      link_level_policy   = "0msDebounce_LLPol"
      link_flap_policy    = "30FlapsOver420"
      monitoring_policy   = "default"
      macsec_policy       = "default"
      l3_interface_policy = "BFD-ISIS-Pol-Enabled-L3IntPol"
      dwdm_policy         = "default"
    }
  ]

  profiles = [
    {
      name        = "Leaf201"
      description = "Leaf201 Fabric Ports"
      port_selectors = [
        {
          name         = "Port10"
          description  = "A description for port 9"
          port_from    = 10
          port_to      = 10
          policy_group = "MyLeaf-PolGrp"
        },
        {
          name         = "Port11"
          description  = "A description for port 10"
          port_from    = 11
          port_to      = 11
          policy_group = "MyLeaf-PolGrp"
        }
      ]
    },
    {
      name           = "Leaf202"
      description    = "Leaf202 Fabric Ports"
      port_selectors = []
    }
  ]
}

module "spine-interfaces" {
  source = "./modules/spine-interfaces"

  policy_groups = [
    {
      name                = "MySpine-PolGrp"
      description         = "My Spine Fabric Port Policy Group"
      link_level_policy   = "0msDebounce_LLPol"
      link_flap_policy    = "30FlapsOver420"
      monitoring_policy   = "default"
      macsec_policy       = "default"
      l3_interface_policy = "BFD-ISIS-Pol-Enabled-L3IntPol"
      dwdm_policy         = "default"
    }
  ]

  profiles = [
    {
      name        = "Spine101-IntPro"
      description = "Spine 101 Interface Profile"
      port_selectors = [
        {
          name         = "Port10"
          description  = "A description for port 10"
          port_from    = 10
          port_to      = 10
          policy_group = "MySpine-PolGrp"
        },
        {
          name         = "Port11"
          description  = "A description for port 11"
          port_from    = 11
          port_to      = 11
          policy_group = "MySpine-PolGrp"
        }
      ]
    },
    {
      name           = "Spine102-IntPro"
      description    = "Spine 102 Interface Profile"
      port_selectors = []
    }
  ]
}

module "leaf-switches" {
  source = "./modules/leaf-switches"

  profiles = [
    {
      name        = "L201-SWPro"
      description = "Leaf 201 Switch Profile"
      switches = [
        {
          name    = "L201"
          from_id = 201
          to_id   = 201
        }
      ]
      interface_profiles = ["Leaf201"]
    },
    {
      name        = "L202-SWPro"
      description = "Leaf 202 Switch Profile"
      switches = [
        {
          name    = "L202"
          from_id = 202
          to_id   = 202
        }
      ]
      interface_profiles = ["Leaf202"]
    }
  ]
}

module "spine-switches" {
  source = "./modules/spine-switches"

  profiles = [
    {
      name        = "S101-SWPro"
      description = "Spine 101 Switch Profile"
      switches = [
        {
          name    = "S101"
          from_id = 101
          to_id   = 101
        }
      ]
      interface_profiles = ["Spine101-IntPro"]
    },
    {
      name        = "S102-SWPro"
      description = "Spine 102 Switch Profile"
      switches = [
        {
          name    = "S102"
          from_id = 102
          to_id   = 102
        }
      ]
      interface_profiles = ["Spine102-IntPro"]
    }
  ]
}
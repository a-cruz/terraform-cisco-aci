terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_rest" "leaf_switch_profile" {
  for_each = {
    for profile in var.profiles : profile.name => profile
  }
  path    = "/api/node/mo/uni/fabric/leprof-${each.value.name}.json"
  payload = <<EOF
    {
        "fabricLeafP": {
            "attributes": {
                "dn": "uni/fabric/leprof-${each.value.name}",
                "name": "${each.value.name}",
                "descr": "${each.value.description}",
                "rn": "leprof-${each.value.name}"
            },
            "children": []
        }
    }
    EOF
}

locals {
  leaf_sw_associations = flatten([
    for profile in var.profiles : [
      for switch in profile.switches : {
        switch_profile = profile.name
        name           = switch.name
        from_id        = switch.from_id
        to_id          = switch.to_id
      }
    ]
  ])
}
# GENERATE A RANDOM ID FOR SWITCH SELECTOR BLOCKS
resource "random_string" "leaf_switch_block_ids" {
  for_each = {
    for switch in local.leaf_sw_associations : "${switch.switch_profile}/${switch.name}" => switch
  }
  length  = 16
  special = false
}
resource "aci_rest" "leaf_switch_association" {
  depends_on = [aci_rest.leaf_switch_profile, random_string.leaf_switch_block_ids]
  for_each = {
    for switch in local.leaf_sw_associations : "${switch.switch_profile}/${switch.name}" => switch
  }
  path    = "/api/node/mo/uni/fabric/leprof-${each.value.switch_profile}/leaves-${each.value.name}-typ-range.json"
  payload = <<EOF
    {
        "fabricLeafS": {
            "attributes": {
                "dn": "uni/fabric/leprof-${each.value.switch_profile}/leaves-${each.value.name}-typ-range",
                "type": "range",
                "name": "${each.value.name}",
                "rn": "leaves-${each.value.name}-typ-range"
            },
            "children": [
                {
                    "fabricNodeBlk": {
                        "attributes": {
                            "dn": "uni/fabric/leprof-${each.value.switch_profile}/leaves-${each.value.name}-typ-range/nodeblk-${random_string.leaf_switch_block_ids["${each.value.switch_profile}/${each.value.name}"].id}",
                            "from_": "${each.value.from_id}",
                            "to_": "${each.value.to_id}",
                            "name": "${random_string.leaf_switch_block_ids["${each.value.switch_profile}/${each.value.name}"].id}",
                            "rn": "nodeblk-${random_string.leaf_switch_block_ids["${each.value.switch_profile}/${each.value.name}"].id}"
                        },
                        "children": []
                    }
                }
            ]
        }
    }
    EOF
}

locals {
  leaf_interface_associations = flatten([
    for profile in var.profiles : [
      for intpro in profile.interface_profiles : {
        switch_profile = profile.name
        name           = intpro
      }
    ]
  ])
}
resource "aci_rest" "leaf_interface_associations" {
  depends_on = [aci_rest.leaf_switch_profile]
  for_each = {
    for association in local.leaf_interface_associations : "${association.switch_profile}/${association.name}" => association
  }
  path    = "/api/node/mo/uni/fabric/leprof-${each.value.switch_profile}/rslePortP-[uni/fabric/leportp-${each.value.name}].json"
  payload = <<EOF
    {
        "fabricRsLePortP": {
            "attributes": {
                "dn": "uni/fabric/leprof-${each.value.switch_profile}/rslePortP-[uni/fabric/leportp-${each.value.name}]"
            },
            "children": []
        }
    }
    EOF
}
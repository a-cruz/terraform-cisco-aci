variable "l3_interface_policies" {
  type = list(map(string))
}

variable "link_level_policies" {
  type = list(object({
    name                        = string
    description                 = string
    link_debounce_msec_interval = number
  }))
}

variable "link_flap_policies" {
  type = list(object({
    name                       = string
    description                = string
    max_flaps_allowed_per_time = number
    time_allowed_seconds       = number
  }))
}
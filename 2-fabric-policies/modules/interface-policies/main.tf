terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_l3_interface_policy" "l3_interface_policies" {
  for_each = {
    for pol in var.l3_interface_policies : pol.name => pol
  }
  name        = each.value.name
  bfd_isis    = each.value.bfd_isis_policy_configuration
  description = each.value.description
}

resource "aci_rest" "link_level_policies" {
  for_each = {
    for pol in var.link_level_policies : pol.name => pol
  }
  path    = "/api/node/mo/uni/fabric/fintfpol-${each.value.name}.json"
  payload = <<EOF
    {
        "fabricFIfPol": {
            "attributes": {
                "dn": "uni/fabric/fintfpol-${each.value.name}",
                "name": "${each.value.name}",
                "descr": "${each.value.description}",
                "linkDebounce": "${each.value.link_debounce_msec_interval}",
                "rn": "fintfpol-${each.value.name}"
            },
            "children": []
        }
    }
    EOF
}

resource "aci_rest" "link_flap_policies" {
  for_each = {
    for pol in var.link_flap_policies : pol.name => pol
  }
  path    = "/api/node/mo/uni/fabric/flinkflappol-${each.value.name}.json"
  payload = <<EOF
    {
        "fabricFLinkFlapPol": {
            "attributes": {
                "dn": "uni/fabric/flinkflappol-${each.value.name}",
                "name": "${each.value.name}",
                "descr": "${each.value.description}",
                "linkFlapErrorMax": "${each.value.max_flaps_allowed_per_time}",
                "linkFlapErrorSeconds":"${each.value.time_allowed_seconds}",
                "rn": "flinkflappol-${each.value.name}"
            },
            "children": []
        }
    }
    EOF
}
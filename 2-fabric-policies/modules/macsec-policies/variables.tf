variable "macsec_parameters" {
  type = list(object({
    name            = string
    description     = string
    cipher_suite    = string
    window_size     = number
    security_policy = string
  }))
}

variable "key_chains" {
  type = list(object({
    name         = string
    description  = string
    key_policies = list(map(string))
  }))
}

variable "interface_policies" {
  type = list(object({
    name              = string
    description       = string
    admin_state       = string
    generate_keys     = bool
    parameters_policy = string
    keychain          = string
  }))
}
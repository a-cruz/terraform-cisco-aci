terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_rest" "macsec_parameter_policy" {
  for_each = {
    for pol in var.macsec_parameters : pol.name => pol
  }
  path    = "/api/node/mo/uni/fabric/macsecpcontfab/fabparamp-${each.value.name}.json"
  payload = <<EOF
    {
        "macsecFabParamPol": {
            "attributes": {
                "dn": "uni/fabric/macsecpcontfab/fabparamp-${each.value.name}",
                "name": "${each.value.name}",
                "descr": "${each.value.description}",
                "cipherSuite": "${each.value.cipher_suite}",
                "replayWindow": "${each.value.window_size}",
                "secPolicy": "${each.value.security_policy}",
                "rn": "fabparamp-${each.value.name}"
            },
            "children": []
        }
    }
    EOF
}

resource "aci_rest" "macsec_keychain_policy" {
  for_each = {
    for pol in var.key_chains : pol.name => pol
  }
  path    = "/api/node/mo/uni/fabric/macsecpcontfab/keychainp-${each.value.name}.json"
  payload = <<EOF
    {
        "macsecKeyChainPol": {
            "attributes": {
                "dn": "uni/fabric/macsecpcontfab/keychainp-${each.value.name}",
                "name": "${each.value.name}",
                "descr": "${each.value.description}",
                "rn": "keychainp-${each.value.name}"
            },
            "children": []
        }
    }
    EOF
}

locals {
  macsec_key_policies = flatten([
    for key_chain in var.key_chains : [
      for pol in key_chain.key_policies : {
        keychain_policy = key_chain.name
        name            = pol.name
        description     = pol.description
        key_name        = pol.key_name
        pre_shared_key  = pol.pre_shared_key
        start_time      = pol.start_time
        end_time        = pol.end_time
      }
    ]
  ])
}
resource "aci_rest" "macsec_key_policy" {
  depends_on = [aci_rest.macsec_keychain_policy]
  for_each = {
    for pol in local.macsec_key_policies : "${pol.keychain_policy}/${pol.name}" => pol
  }
  path    = "/api/node/mo/uni/fabric/macsecpcontfab/keychainp-${each.value.keychain_policy}/keyp-${each.value.key_name}.json"
  payload = <<EOF
    {
        "macsecKeyPol": {
            "attributes": {
                "dn": "uni/fabric/macsecpcontfab/keychainp-${each.value.keychain_policy}/keyp-${each.value.key_name}",
                "name": "${each.value.name}",
                "descr": "${each.value.description}",
                "keyName": "${each.value.key_name}",
                "preSharedKey": "${each.value.pre_shared_key}",
                "startTime": "${each.value.start_time}",
                "endTime": "${each.value.end_time}",
                "rn": "keyp-${each.value.key_name}"
            },
            "children": []
        }
    }
    EOF
}

resource "aci_rest" "macsec_interface_policy" {
  depends_on = [aci_rest.macsec_parameter_policy]
  for_each = {
    for pol in var.interface_policies : pol.name => pol
  }
  path    = "/api/node/mo/uni/fabric/macsecfabifp-${each.value.name}.json"
  payload = <<EOF
    {
        "macsecFabIfPol": {
            "attributes": {
                "dn": "uni/fabric/macsecfabifp-${each.value.name}",
                "name": "${each.value.name}",
                "descr": "${each.value.description}",
                "useAutoKeys": "${each.value.generate_keys}",
                "rn": "macsecfabifp-${each.value.name}"
            },
            "children": [
                {
                    "macsecRsToParamPol": {
                        "attributes": {
                            "tDn": "uni/fabric/macsecpcontfab/fabparamp-${each.value.parameters_policy}"
                        },
                        "children": []
                    }
                }
            ]
        }
    }
    EOF
}

resource "aci_rest" "macsec_add_keychain_to_int_pol" {
  depends_on = [aci_rest.macsec_keychain_policy]
  for_each = {
    for pol in var.interface_policies : pol.name => pol if pol.generate_keys == false
  }
  path    = "/api/node/mo/uni/fabric/macsecfabifp-${each.value.name}.json"
  payload = <<EOF
    {
        "macsecFabIfPol": {
            "attributes": {},
            "children": [
                {
                    "macsecRsToKeyChainPol": {
                        "attributes": {
                            "tDn": "uni/fabric/macsecpcontfab/keychainp-${each.value.keychain}"
                        },
                        "children": []
                    }
                }
            ]
        }
    }
    EOF
}
terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_rest" "spine_switch_profile" {
  for_each = {
    for profile in var.profiles : profile.name => profile
  }
  path    = "/api/node/mo/uni/fabric/spprof-${each.value.name}.json"
  payload = <<EOF
    {
        "fabricSpineP": {
            "attributes": {
                "dn": "uni/fabric/spprof-${each.value.name}",
                "name": "${each.value.name}",
                "descr": "${each.value.description}",
                "rn": "spprof-${each.value.name}"
            },
            "children": []
        }
    }
    EOF
}

locals {
  spine_sw_associations = flatten([
    for profile in var.profiles : [
      for switch in profile.switches : {
        switch_profile = profile.name
        name           = switch.name
        from_id        = switch.from_id
        to_id          = switch.to_id
      }
    ]
  ])
}
# GENERATE A RANDOM ID FOR SWITCH SELECTOR BLOCKS
resource "random_string" "spine_switch_block_ids" {
  for_each = {
    for switch in local.spine_sw_associations : "${switch.switch_profile}/${switch.name}" => switch
  }
  length  = 16
  special = false
}
resource "aci_rest" "spine_switch_association" {
  depends_on = [aci_rest.spine_switch_profile, random_string.spine_switch_block_ids]
  for_each = {
    for switch in local.spine_sw_associations : "${switch.switch_profile}/${switch.name}" => switch
  }
  path    = "/api/node/mo/uni/fabric/spprof-${each.value.switch_profile}/spines-${each.value.name}-typ-range.json"
  payload = <<EOF
    {
        "fabricSpineS": {
            "attributes": {
                "dn": "uni/fabric/spprof-${each.value.switch_profile}/spines-${each.value.name}-typ-range",
                "type": "range",
                "name": "${each.value.name}",
                "rn": "spines-${each.value.name}-typ-range"
            },
            "children": [
                {
                    "fabricNodeBlk": {
                        "attributes": {
                            "dn": "uni/fabric/spprof-${each.value.switch_profile}/spines-${each.value.name}-typ-range/nodeblk-${random_string.spine_switch_block_ids["${each.value.switch_profile}/${each.value.name}"].id}",
                            "from_": "${each.value.from_id}",
                            "to_": "${each.value.to_id}",
                            "name": "${random_string.spine_switch_block_ids["${each.value.switch_profile}/${each.value.name}"].id}",
                            "rn": "nodeblk-${random_string.spine_switch_block_ids["${each.value.switch_profile}/${each.value.name}"].id}"
                        },
                        "children": []
                    }
                }
            ]
        }
    }
    EOF
}

locals {
  spine_interface_associations = flatten([
    for profile in var.profiles : [
      for intpro in profile.interface_profiles : {
        switch_profile = profile.name
        name           = intpro
      }
    ]
  ])
}
resource "aci_rest" "spine_interface_associations" {
  depends_on = [aci_rest.spine_switch_profile]
  for_each = {
    for association in local.spine_interface_associations : "${association.switch_profile}/${association.name}" => association
  }
  path    = "/api/node/mo/uni/fabric/spprof-${each.value.switch_profile}/rsspPortP-[uni/fabric/spportp-${each.value.name}].json"
  payload = <<EOF
    {
        "fabricRsSpPortP": {
            "attributes": {
                "dn": "uni/fabric/spprof-${each.value.switch_profile}/rsspPortP-[uni/fabric/spportp-${each.value.name}]"
            },
            "children": []
        }
    }
    EOF
}
variable "profiles" {
  type = list(object({
    name        = string
    description = string
    switches = list(object({
      name    = string
      from_id = number
      to_id   = number
    }))
    interface_profiles = list(string)
  }))
}
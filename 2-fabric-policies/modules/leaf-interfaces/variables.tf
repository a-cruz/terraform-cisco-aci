variable "policy_groups" {
  type = list(map(string))
}

variable "profiles" {
  type = list(object({
    name        = string
    description = string
    port_selectors = list(object({
      name         = string
      description  = string
      port_from    = number
      port_to      = number
      policy_group = string
    }))
  }))
}
terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_rest" "leaf_policy_group" {
  for_each = {
    for polgrp in var.policy_groups : polgrp.name => polgrp
  }
  path    = "/api/node/mo/uni/fabric/funcprof/leportgrp-${each.value.name}.json"
  payload = <<EOF
    {
        "fabricLePortPGrp": {
            "attributes": {
                "dn": "uni/fabric/funcprof/leportgrp-${each.value.name}",
                "name": "${each.value.name}",
                "descr": "${each.value.description}",
                "rn": "leportgrp-${each.value.name}"
            },
            "children": [
                {
                    "fabricRsFIfPol": {
                        "attributes": {
                            "tnFabricFIfPolName": "${each.value.link_level_policy}"
                        },
                        "children": []
                    }
                },
                {
                    "fabricRsFLinkFlapPol": {
                        "attributes": {
                            "tnFabricFLinkFlapPolName": "${each.value.link_flap_policy}"
                        },
                        "children": []
                    }
                },
                {
                    "fabricRsMonIfFabricPol": {
                        "attributes": {
                            "tnMonFabricPolName": "${each.value.monitoring_policy}"
                        },
                        "children": []
                    }
                },
                {
                    "fabricRsMacsecFabIfPol": {
                        "attributes": {
                            "tnMacsecFabIfPolName": "${each.value.macsec_policy}"
                        },
                        "children": []
                    }
                },
                {
                    "fabricRsL3IfPol": {
                        "attributes": {
                            "tnL3IfPolName": "${each.value.l3_interface_policy}"
                        },
                        "children": []
                    }
                },
                {
                    "fabricRsDwdmFabIfPol": {
                        "attributes": {
                            "tnDwdmFabIfPolName": "${each.value.dwdm_policy}"
                        },
                        "children": []
                    }
                },
                {
                    "fabricRsOpticsFabIfPol": {
                        "attributes": {
                            "tDn": "uni/fabric/zrfab-unspecified"
                        },
                        "children": []
                    }
                }
            ]
        }
    }
    EOF
}

resource "aci_rest" "leaf_profile" {
  for_each = {
    for profile in var.profiles : profile.name => profile
  }
  path    = "/api/node/mo/uni/fabric/leportp-${each.value.name}.json"
  payload = <<EOF
    {
        "fabricLePortP": {
            "attributes": {
                "dn": "uni/fabric/leportp-${each.value.name}",
                "name": "${each.value.name}",
                "descr": "${each.value.description}",
                "rn": "leportp-${each.value.name}"
            },
            "children": []
        }
    }
    EOF
}

locals {
  port_selectors = flatten([
    for profile in var.profiles : [
      for selector in profile.port_selectors : {
        interface_profile = profile.name
        name              = selector.name
        description       = selector.description
        port_from         = selector.port_from
        port_to           = selector.port_to
        policy_group      = selector.policy_group
      }
    ]
  ])
}
# GENERATE A RANDOM ID FOR PORT SELECTOR BLOCKS
resource "random_string" "port_block_ids" {
  for_each = {
    for selector in local.port_selectors : "${selector.interface_profile}/${selector.name}" => selector
  }
  length  = 16
  special = false
}
resource "aci_rest" "leaf_port_selector" {
  depends_on = [aci_rest.leaf_profile, aci_rest.leaf_policy_group]
  for_each = {
    for selector in local.port_selectors : "${selector.interface_profile}/${selector.name}" => selector
  }
  path    = "/api/node/mo/uni/fabric/leportp-${each.value.interface_profile}/lefabports-${each.value.name}-typ-range.json"
  payload = <<EOF
    {
        "fabricLFPortS": {
            "attributes": {
                "dn": "uni/fabric/leportp-${each.value.interface_profile}/lefabports-${each.value.name}-typ-range",
                "type": "range",
                "name": "${each.value.name}",
                "descr": "${each.value.description}",
                "rn": "lefabports-${each.value.name}-typ-range"
            },
            "children": [
                {
                    "fabricPortBlk": {
                        "attributes": {
                            "dn": "uni/fabric/leportp-${each.value.interface_profile}/lefabports-${each.value.name}-typ-range/portblk-${random_string.port_block_ids["${each.value.interface_profile}/${each.value.name}"].id}",
                            "fromPort": "${each.value.port_from}",
                            "toPort": "${each.value.port_to}",
                            "name": "${random_string.port_block_ids["${each.value.interface_profile}/${each.value.name}"].id}",
                            "rn": "portblk-${random_string.port_block_ids["${each.value.interface_profile}/${each.value.name}"].id}"
                        },
                        "children": []
                    }
                },
                {
                    "fabricRsLePortPGrp": {
                        "attributes": {
                            "tDn": "uni/fabric/funcprof/leportgrp-${each.value.policy_group}"
                        },
                        "children": []
                    }
                }
            ]
        }
    }
    EOF
}
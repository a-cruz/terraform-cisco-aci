variable "date_and_time_policies" {
  type = list(object({
    name                 = string
    admin_state          = string
    server_state         = string
    master_mode          = string
    stratum_value        = number
    authentication_state = string
    ntp_auth_keys = list(object({
      id        = number
      key       = string
      trusted   = bool
      auth_type = string
    }))
    ntp_servers = list(object({
      server            = string
      description       = string
      preferred         = bool
      min_poll_interval = number
      max_poll_interval = number
      mgmt_epg          = string
    }))
  }))
}

variable "snmp_policies" {
  type = list(object({
    name              = string
    description       = string
    admin_state       = string
    community_strings = list(map(string))
    snmp_v3_users     = list(map(string))
    client_group_profiles = list(object({
      name           = string
      description    = string
      mgmt_epg       = string
      client_entries = list(map(string))
    }))
    trap_forward_servers = list(object({
      ip_address = string
      port       = number
    }))
  }))
}
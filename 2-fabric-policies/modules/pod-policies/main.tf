terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

# DATE AND TIME POLICIES
resource "aci_rest" "date_time_policy" {
  for_each = {
    for pol in var.date_and_time_policies : pol.name => pol
  }

  path    = "/api/node/mo/uni/fabric/time-${each.value.name}.json"
  payload = <<EOF
    {
        "datetimePol": {
            "attributes": {
                "dn": "uni/fabric/time-${each.value.name}",
                "name": "${each.value.name}",
                "adminSt": "${each.value.admin_state}",
                "serverState": "${each.value.server_state}",
                "masterMode": "${each.value.master_mode}",
                "StratumValue": "${each.value.stratum_value}",
                "authSt": "${each.value.authentication_state}",
                "rn": "time-${each.value.name}"
            },
            "children": []
        }
    }
    EOF
}

locals {
  ntp_auth_keys = flatten([
    for pol in var.date_and_time_policies : [
      for auth_key in pol.ntp_auth_keys : {
        date_time_policy = pol.name
        id               = auth_key.id
        key              = auth_key.key
        auth_type        = auth_key.auth_type
        trusted          = auth_key.trusted
      }
    ]
  ])
}
resource "aci_rest" "ntp_auth_keys" {
  depends_on = [aci_rest.date_time_policy]
  for_each = {
    for auth_key in local.ntp_auth_keys : "${auth_key.date_time_policy}/${auth_key.id}" => auth_key
  }
  path    = "/api/node/mo/uni/fabric/time-${each.value.date_time_policy}/ntpauth-${each.value.id}.json"
  payload = <<EOF
    {
        "datetimeNtpAuthKey": {
            "attributes": {
                "dn": "uni/fabric/time-${each.value.date_time_policy}/ntpauth-${each.value.id}",
                "id": "${each.value.id}",
                "key": "${each.value.key}",
                "keyType": "${each.value.auth_type}",
                "trusted": "${each.value.trusted}",
                "rn": "ntpauth-${each.value.id}"
            },
            "children": []
        }
    }
    EOF
}

locals {
  ntp_servers = flatten([
    for pol in var.date_and_time_policies : [
      for server in pol.ntp_servers : {
        date_time_policy = pol.name
        name             = server.server
        description      = server.description
        preferred        = server.preferred
        min_poll         = server.min_poll_interval
        max_poll         = server.max_poll_interval
        mgmt_epg         = server.mgmt_epg
      }
    ]
  ])
}
resource "aci_rest" "ntp_servers" {
  depends_on = [aci_rest.date_time_policy]
  for_each = {
    for server in local.ntp_servers : "${server.date_time_policy}/${server.name}" => server
  }
  path    = "/api/node/mo/uni/fabric/time-${each.value.date_time_policy}/ntpprov-${each.value.name}.json"
  payload = <<EOF
    {
        "datetimeNtpProv": {
            "attributes": {
                "dn": "uni/fabric/time-${each.value.date_time_policy}/ntpprov-${each.value.name}",
                "name": "${each.value.name}",
                "descr": "${each.value.description}",
                "preferred": "${each.value.preferred}",
                "minPoll": "${each.value.min_poll}",
                "maxPoll": "${each.value.max_poll}",
                "rn": "ntpprov-${each.value.name}"
            },
            "children": [
                {
                    "datetimeRsNtpProvToEpg": {
                        "attributes": {
                            "tDn": "uni/tn-mgmt/mgmtp-${each.value.mgmt_epg}/oob-${each.value.mgmt_epg}"
                        },
                        "children": []
                    }
                }
            ]
        }
    }
    EOF
}

# SNMP POLICIES
resource "aci_rest" "snmp_policy" {
  for_each = {
    for pol in var.snmp_policies : pol.name => pol
  }
  path    = "/api/node/mo/uni/fabric/snmppol-${each.value.name}.json"
  payload = <<EOF
    {
        "snmpPol": {
            "attributes": {
                "dn": "uni/fabric/snmppol-${each.value.name}",
                "name": "${each.value.name}",
                "descr": "${each.value.description}",
                "adminSt": "${each.value.admin_state}",
                "rn": "snmppol-${each.value.name}"
            },
            "children": []
        }
    }
    EOF
}

locals {
  snmp_community_strings = flatten([
    for pol in var.snmp_policies : [
      for cstring in pol.community_strings : {
        snmp_policy      = pol.name
        community_string = cstring.community_string
        description      = cstring.description
      }
    ]
  ])
}
resource "aci_rest" "snmp_community" {
  depends_on = [aci_rest.snmp_policy]
  for_each = {
    for cstring in local.snmp_community_strings : "${cstring.snmp_policy}/${cstring.community_string}" => cstring
  }
  path    = "/api/node/mo/uni/fabric/snmppol-${each.value.snmp_policy}/community-${each.value.community_string}.json"
  payload = <<EOF
    {
        "snmpCommunityP": {
            "attributes": {
                "dn": "uni/fabric/snmppol-${each.value.snmp_policy}/community-${each.value.community_string}",
                "name": "${each.value.community_string}",
                "descr": "${each.value.description}",
                "rn": "community-${each.value.community_string}"
            },
            "children": []
        }
    }
    EOF
}

locals {
  snmp_v3_users = flatten([
    for pol in var.snmp_policies : [
      for user in pol.snmp_v3_users : {
        snmp_policy  = pol.name
        name         = user.name
        privacy_type = user.privacy_type
        privacy_key  = user.privacy_key
        auth_type    = user.auth_type
        auth_key     = user.auth_key
      }
    ]
  ])
}
resource "aci_rest" "snmp_v3_user" {
  depends_on = [aci_rest.snmp_policy]
  for_each = {
    for user in local.snmp_v3_users : "${user.snmp_policy}/${user.name}" => user
  }
  path    = "/api/node/mo/uni/fabric/snmppol-${each.value.snmp_policy}/user-${each.value.name}.json"
  payload = <<EOF
    {
        "snmpUserP": {
            "attributes": {
                "dn": "uni/fabric/snmppol-${each.value.snmp_policy}/user-${each.value.name}",
                "name": "${each.value.name}",
                "privType": "${each.value.privacy_type}",
                "privKey": "${each.value.privacy_key}",
                "authType": "${each.value.auth_type}",
                "authKey": "${each.value.auth_key}",
                "rn": "user-${each.value.name}"
            },
            "children": []
        }
    }
    EOF
}

locals {
  snmp_client_group_profiles = flatten([
    for pol in var.snmp_policies : [
      for profile in pol.client_group_profiles : {
        snmp_policy = pol.name
        name        = profile.name
        description = profile.description
        mgmt_epg    = profile.mgmt_epg
      }
    ]
  ])
}
resource "aci_rest" "snmp_client_group_profile" {
  depends_on = [aci_rest.snmp_policy]
  for_each = {
    for profile in local.snmp_client_group_profiles : "${profile.snmp_policy}/${profile.name}" => profile
  }
  path    = "/api/node/mo/uni/fabric/snmppol-${each.value.snmp_policy}/clgrp-${each.value.name}.json"
  payload = <<EOF
    {
        "snmpClientGrpP": {
            "attributes": {
                "dn": "uni/fabric/snmppol-${each.value.snmp_policy}/clgrp-${each.value.name}",
                "name": "${each.value.name}",
                "descr": "${each.value.description}",
                "rn": "clgrp-${each.value.name}"
            },
            "children": [
                {
                    "snmpRsEpg": {
                        "attributes": {
                            "tDn": "uni/tn-mgmt/mgmtp-default/oob-${each.value.mgmt_epg}"
                        },
                        "children": []
                    }
                }
            ]
        }
    }
    EOF
}

locals {
  snmp_client_entries = flatten([
    for pol in var.snmp_policies : [
      for profile in pol.client_group_profiles : [
        for client in profile.client_entries : {
          snmp_policy               = pol.name
          snmp_client_group_profile = profile.name
          name                      = client.name
          ip_address                = client.ip_address
        }
      ]
    ]
  ])
}
resource "aci_rest" "snmp_client_entry" {
  depends_on = [aci_rest.snmp_policy, aci_rest.snmp_client_group_profile]
  for_each = {
    for client in local.snmp_client_entries : "${client.snmp_policy}/${client.snmp_client_group_profile}/${client.name}" => client
  }
  path    = "/api/node/mo/uni/fabric/snmppol-${each.value.snmp_policy}/clgrp-${each.value.snmp_client_group_profile}/client-[${each.value.ip_address}].json"
  payload = <<EOF
    {
        "snmpClientP": {
            "attributes": {
                "dn": "uni/fabric/snmppol-${each.value.snmp_policy}/clgrp-${each.value.snmp_client_group_profile}/client-[${each.value.ip_address}]",
                "name": "${each.value.name}",
                "addr": "${each.value.ip_address}",
                "rn": "client-[${each.value.ip_address}]"
            },
            "children": []
        }
    }
    EOF
}

locals {
  snmp_trap_servers = flatten([
    for pol in var.snmp_policies : [
      for server in pol.trap_forward_servers : {
        snmp_policy = pol.name
        ip_address  = server.ip_address
        port        = server.port
      }
    ]
  ])
}
resource "aci_rest" "snmp_trap_forward_server" {
  depends_on = [aci_rest.snmp_policy]
  for_each = {
    for server in local.snmp_trap_servers : "${server.snmp_policy}/${server.ip_address}" => server
  }
  path    = "/api/node/mo/uni/fabric/snmppol-${each.value.snmp_policy}/trapfwdserver-[${each.value.ip_address}].json"
  payload = <<EOF
    {
        "snmpTrapFwdServerP": {
            "attributes": {
                "dn": "uni/fabric/snmppol-${each.value.snmp_policy}/trapfwdserver-[${each.value.ip_address}]",
                "addr": "${each.value.ip_address}",
                "port": "${each.value.port}",
                "rn": "trapfwdserver-[${each.value.ip_address}]"
            },
            "children": []
        }
    }
    EOF
}
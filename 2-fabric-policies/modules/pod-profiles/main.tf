terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_rest" "pod_policy_group" {
  for_each = {
    for group in var.pod_policy_groups : group.name => group
  }
  path    = "/api/node/mo/uni/fabric/funcprof/podpgrp-${each.value.name}.json"
  payload = <<EOF
    {
        "fabricPodPGrp": {
            "attributes": {
                "dn": "uni/fabric/funcprof/podpgrp-${each.value.name}",
                "name": "${each.value.name}",
                "descr": "${each.value.description}",
                "rn": "podpgrp-${each.value.name}"
            },
            "children": [
                {
                    "fabricRsTimePol": {
                        "attributes": {
                            "tnDatetimePolName": "${each.value.date_time_policy}",
                            "status": "created,modified"
                        },
                        "children": []
                    }
                },
                {
                    "fabricRsPodPGrpIsisDomP": {
                        "attributes": {
                            "tnIsisDomPolName": "${each.value.isis_policy}",
                            "status": "created,modified"
                        },
                        "children": []
                    }
                },
                {
                    "fabricRsPodPGrpCoopP": {
                        "attributes": {
                            "tnCoopPolName": "${each.value.isis_policy}",
                            "status": "created,modified"
                        },
                        "children": []
                    }
                },
                {
                    "fabricRsPodPGrpBGPRRP": {
                        "attributes": {
                            "tnBgpInstPolName": "${each.value.bgp_rr_policy}",
                            "status": "created,modified"
                        },
                        "children": []
                    }
                },
                {
                    "fabricRsCommPol": {
                        "attributes": {
                            "tnCommPolName": "${each.value.mgmt_access_policy}",
                            "status": "created,modified"
                        },
                        "children": []
                    }
                },
                {
                    "fabricRsSnmpPol": {
                        "attributes": {
                            "tnSnmpPolName": "${each.value.snmp_policy}",
                            "status": "created,modified"
                        },
                        "children": []
                    }
                },
                {
                    "fabricRsMacsecPol": {
                        "attributes": {
                            "tnMacsecFabIfPolName": "${each.value.macsec_policy}",
                            "status": "created,modified"
                        },
                        "children": []
                    }
                }
            ]
        }
    }
    EOF
}

resource "aci_rest" "pod_profile" {
  for_each = {
    for profile in var.pod_profiles : profile.name => profile
  }
  path    = "/api/node/mo/uni/fabric/podprof-${each.value.name}.json"
  payload = <<EOF
    {
        "fabricPodP": {
            "attributes": {
                "dn": "uni/fabric/podprof-${each.value.name}",
                "name": "${each.value.name}",
                "descr": "${each.value.description}",
                "rn": "podprof-${each.value.name}"
            },
            "children": []
        }
    }
    EOF
}

locals {
  pod_selectors = flatten([
    for profile in var.pod_profiles : [
      for selector in profile.pod_selectors : {
        profile_name = profile.name
        name         = selector.name
        type         = selector.type
        from_id      = selector.type == "range" ? split("-", selector.range)[0] : null
        to_id        = selector.type == "range" ? length(split("-", selector.range)) == 1 ? split("-", selector.range)[0] : split("-", selector.range)[1] : null
        policy_group = selector.policy_group
      }
    ]
  ])
}
resource "aci_rest" "pod_selector_type_all" {
  for_each = {
    for selector in local.pod_selectors : "${selector.profile_name}/${selector.name}" => selector if selector.type == "ALL"
  }
  depends_on = [aci_rest.pod_policy_group, aci_rest.pod_profile]
  path       = "/api/node/mo/uni/fabric/podprof-${each.value.profile_name}/pods-${each.value.name}-typ-ALL.json"
  payload    = <<EOF
    {
        "fabricPodS": {
            "attributes": {
                "dn": "uni/fabric/podprof-${each.value.profile_name}/pods-${each.value.name}-typ-ALL",
                "name": "${each.value.name}",
                "type": "ALL",
                "rn": "pods-${each.value.name}-typ-ALL"
            },
            "children": [
                {
                    "fabricRsPodPGrp": {
                        "attributes": {
                            "tDn": "uni/fabric/funcprof/podpgrp-${each.value.policy_group}",
                            "status": "created"
                        },
                        "children": []
                    }
                }
            ]
        }
    }
    EOF
}

# GENERATE A RANDOM ID FOR POD SELECTOR BLOCKS
resource "random_string" "pod_block_ids" {
  for_each = {
    for selector in local.pod_selectors : "${selector.profile_name}/${selector.name}" => selector if selector.type == "range"
  }
  length  = 16
  special = false
}

resource "aci_rest" "pod_selector_type_range" {
  depends_on = [aci_rest.pod_policy_group, aci_rest.pod_profile]
  for_each = {
    for selector in local.pod_selectors : "${selector.profile_name}/${selector.name}" => selector if selector.type == "range"
  }
  path    = "/api/node/mo/uni/fabric/podprof-${each.value.profile_name}/pods-${each.value.name}-typ-range.json"
  payload = <<EOF
    {
        "fabricPodS": {
            "attributes": {
                "dn": "uni/fabric/podprof-${each.value.profile_name}/pods-${each.value.name}-typ-range",
                "name": "${each.value.name}",
                "type": "range",
                "rn": "pods-${each.value.profile_name}-typ-range"
            },
            "children": [
                {
                    "fabricRsPodPGrp": {
                        "attributes": {
                            "tDn": "uni/fabric/funcprof/podpgrp-${each.value.policy_group}"
                        },
                        "children": []
                    }
                },
                {
                    "fabricPodBlk": {
                        "attributes": {
                            "dn": "uni/fabric/podprof-${each.value.profile_name}/pods-${each.value.name}-typ-range/podblk-${random_string.pod_block_ids["${each.value.profile_name}/${each.value.name}"].id}",
                            "from_": "${each.value.from_id}",
                            "to_": "${each.value.to_id}",
                            "name": "${random_string.pod_block_ids["${each.value.profile_name}/${each.value.name}"].id}",
                            "rn": "podblk-${random_string.pod_block_ids["${each.value.profile_name}/${each.value.name}"].id}"
                        },
                        "children": []
                    }
                }
            ]
        }
    }
    EOF
}
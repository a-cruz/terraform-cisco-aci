variable "pod_policy_groups" {
  type = list(map(string))
}

variable "pod_profiles" {
  type = list(object({
    name          = string
    description   = string
    pod_selectors = list(map(string))
  }))
}
terraform {
  required_providers {
    aci = {
      source = "CiscoDevNet/aci"
    }
  }
}

resource "aci_rest" "fabric_l2_mtu" {
  path    = "/api/node/mo/uni/fabric/l2pol-default.json"
  payload = <<EOF
    {
        "l2InstPol": {
            "attributes": {
                "dn": "uni/fabric/l2pol-default",
                "fabricMtu": "${var.fabric_l2_mtu}"
            },
            "children": []
        }
    }
    EOF
}
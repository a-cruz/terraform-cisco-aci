# dcnet-terraform-cisco-aci
Collection of Terraform plans for deploying Cisco ACI

# REQUIREMENTS
- **Terraform** (Tested with v1.3.9)
- Terraform **CiscoDevNet/aci provider** (Tested with v2.6.1)
- **Cisco ACI** (Tested with APIC Simulator version 5.2.7f)
- Network connectivity (HTTPS) between the system you run the terraform commands and the Cisco ACI APIC

# NOTES
The collection consists of four Terraform plans:
- **1-fabric-bringup**    (Fabric Membership & Fabric-Wide Settings)
- **2-fabric-policies**   (POD and fabric policies)
- **3-access-policies**   (Interface profiles, leaf switch profiles, etc.)
- **4-tenants**           (All tenant configurations)

Plans should generally be applied in that order, though not all are required. The only hard dependencies are tenants & epg-static-paths both assume objects from access-policies are created.

APIC hostname/ip, username, and password are not hard-coded in the plans so you will either need to add those to the plan, supply them at runtime with cli variables, or supply them as environment variables.

To supply credentials as cli variables during terraform apply, do this:

    terraform apply -var="apic_ip=<ip>" -var="apic_user=<user>" -var="apic_pwd=<password>"

To supply credentials as environment variables do this from your Linux shell:
```bash
export TF_VAR_apic_ip=<ip>
export TF_VAR_apic_user=<user>
export TF_VAR_apic_pwd=<password>
```
Alternatively if you don't mind your password sitting in a text file (lab environment), you can put the exports in a text file such as "environment.env" and then when you start your shell session you can load the environment variables from the text file with the command: ```source environment.env```